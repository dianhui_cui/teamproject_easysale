﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckoutTerminal
{
    /// <summary>
    /// Interaction logic for UCPayInCash.xaml
    /// </summary>
    public partial class UCPayInCash : UserControl
    {
        public UCPayInCash()
        {
            InitializeComponent();
            lblTotal.Content = Total;
            lblChange.Content = 0;
        }
        public MainWindow MainWindow
        {
            get; set;
        }
        public double Total
        {
            get; set;
        }
        double change;
        private void btPay_Click(object sender, RoutedEventArgs e)
        {
            if (change >= 0)
            {
                MainWindow.PrintOrder(UCPayment.PayMethod.Cash);
                MainWindow.ClearUC();
                MainWindow.ClearList();
            } else
            {
                MessageBoxResult result = MessageBox.Show("Insufficient Payment, do you want to modify current order?\n"
               , "Error", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                tbPayment.Text = "";
                if (result == MessageBoxResult.OK)
                {
                    MainWindow.ClearUC();
                }
                else if (result == MessageBoxResult.No)
                {
                    return;
                }
            }
        }

        private void tbPayment_TextChanged(object sender, TextChangedEventArgs e)
        {
            double payment;
            if (tbPayment.Text != "")
            {
                if (!double.TryParse(tbPayment.Text, out payment))
                {
                    MessageBox.Show("Wrong weight");
                    return;
                }
                change = (payment - Total);
                lblChange.Content = "$" + change;
            }
        }
    }
}
