﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckoutTerminal
{
    /// <summary>
    /// Interaction logic for UCPayment.xaml
    /// </summary>
    public partial class UCPayment : UserControl
    {
        public UCPayment()
        {
            InitializeComponent();
            rbCash.IsChecked = true;
        }
        public enum PayMethod { Credit, Debit, ApplePay, Cash }
        PayMethod payMethod = PayMethod.Cash;
        public MainWindow MainWindow { get; set; }
        public PayMethod Data
        {
            // string barcode = from commodity in commoditydatabase where Name = commName select commodity.Barcode
            // CommCode
            // price
            // subtotal: lblSubtotal.Content = price * weight

            get
            {
                return payMethod;
            }
            //set { tbWeight.Text = value; }
        }

        /// <summary>
        /// Event to indicate new data is available
        /// </summary>
        public event EventHandler DataAvailable;
        /// <summary>
        /// Called to signal to subscribers that new data is available
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnDataAvailable(EventArgs e)
        {
            EventHandler eh = DataAvailable;
            if (eh != null)
            {
                eh(this, e);
            }
        }

        private void btPay_Click(object sender, RoutedEventArgs e)
        {
            if (rbCredit.IsChecked == true)
            {
                payMethod = PayMethod.Credit;
            } else if (rbDebit.IsChecked == true)
            {
                payMethod = PayMethod.Debit;
            } else if (rbApplePay.IsChecked == true)
            {
                payMethod = PayMethod.ApplePay;
            } else if (rbCash.IsChecked == true)
            {
                payMethod = PayMethod.Cash;
                MainWindow.AddUCPayInCash();
                return;
            } else
            {
                MessageBox.Show("Please choose a payment method");
                return;
            }
            MainWindow.PrintOrder(payMethod);
            MainWindow.ClearUC();
            MainWindow.ClearList();
            //OnDataAvailable(null);
        }
    }
}
