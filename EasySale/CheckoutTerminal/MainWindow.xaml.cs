﻿using EasyModel.business;
using EasySaleDataProvider;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckoutTerminal
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ILog log = log4net.LogManager.GetLogger(typeof(MainWindow));
        UCBarcodeInput ucBarcode = new UCBarcodeInput();
        UCWeigh ucWeight = new UCWeigh();
        
        UCPayment ucPayment = new UCPayment();
        UCPayInCash ucPayInCash = new UCPayInCash();
        List<EasyModel.business.OrderItem> commoditiesList = new List<EasyModel.business.OrderItem>();
        const string FileName = @"..\..\order.txt";
        public MainWindow()
        {
            InitializeComponent();
            //spCommoditySearch.Items.Add(new UCBarcodeInput());
            spCommoditySearch.Children.Add(ucBarcode);
            btTerminateAndPay.IsEnabled = false;
           /* EasyModel.business.OrderItem c1 = new EasyModel.business.OrderItem { CommID = 111111, CommName = "Banana", ItemCount = 1, UnitPrice = 0.69, SubTotal=0.69 };
            EasyModel.business.OrderItem c2 = new EasyModel.business.OrderItem { CommID = 111112, CommName = "Apple", ItemCount = 1, UnitPrice = 0.69, SubTotal = 0.69 };
            EasyModel.business.OrderItem c3 = new EasyModel.business.OrderItem { CommID = 111113, CommName = "Tomato", ItemCount = 1, UnitPrice = 0.99, SubTotal = 0.99 };
            EasyModel.business.OrderItem c4 = new EasyModel.business.OrderItem { CommID = 111116, CommName = "Pear", ItemCount = 3.3, UnitPrice = 0.99, SubTotal = 0.99, GST=0.05, PST=0.09975 };

            commoditiesList.Add(c1);
            commoditiesList.Add(c2);
            commoditiesList.Add(c3);
            commoditiesList.Add(c4);
            CalcSubtotal();*/
            lvCommodities.ItemsSource = commoditiesList;
            btEnterBarcode_Click(null, null);
        }
        
        private void btWeight_Click(object sender, RoutedEventArgs e)
        {
            spCommoditySearch.Children.Clear();
            ucWeight.MainWindow = this;
            
            ucWeight.IsControlsVisible(false);
            spCommoditySearch.Children.Add(ucWeight);
            
            //ucWeight.DataAvailable += new EventHandler(child_DataAvailable);
            //btWeight.IsEnabled = false;
            //btEnterBarcode.IsEnabled = false;
        }
        double gst = 0;
        double qst = 0;
        double subtotal = 0;
        double total = 0;
        void CalcSubtotal()
        {
            gst = 0;
            qst = 0;
            subtotal = 0;
            total = 0;

            foreach (var c in commoditiesList)
            {
                gst += c.GST * c.UnitPrice * c.ItemCount;
                qst += c.PST * c.UnitPrice * c.ItemCount;
                subtotal += c.UnitPrice * c.ItemCount;
                subtotal = Math.Truncate(100 * subtotal) / 100;
                total += gst + qst + subtotal;
                total = Math.Truncate(100 * total) / 100;
            }
            lblGST.Content = string.Format("${0:0.00}", gst);
            lblQST.Content = string.Format("${0:0.00}", qst);
            lblSubtotal.Content = string.Format("${0:0.00}", subtotal);
            lblTotal.Content = string.Format("${0:0.00}", total);
            btTerminateAndPay.IsEnabled = true;
        }
        /// The child has data available - get it.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /*void child_DataAvailable(object sender, EventArgs e)
        {
            if (spCommoditySearch.Children.Contains(ucWeight))
            {
                if (ucWeight.Data != null)
                {
                    //EasyModel.business.Commodity comm = ucWeight.Data;
                    commoditiesList.Add(ucWeight.Data);
                    CalcSubtotal();
                    lvCommodities.Items.Refresh();
                }
                else
                {
                    spCommoditySearch.Children.Clear();
                    ucWeight.DataAvailable -= new EventHandler(child_DataAvailable);
                    btWeight.IsEnabled = true;
                    btEnterBarcode.IsEnabled = false;
                    btEnterBarcode_Click(sender, null);
                }
            } else if (spCommoditySearch.Children.Contains(ucBarcode))
            {
                if (ucBarcode.Data != null) {
                    //EasyModel.business.Commodity comm = ucBarcode.Data;
                    commoditiesList.Add(ucBarcode.Data);
                    CalcSubtotal();
                    lvCommodities.Items.Refresh();
                } else
                {
                    spCommoditySearch.Children.Clear();
                    ucBarcode.DataAvailable -= new EventHandler(child_DataAvailable);
                    btWeight.IsEnabled = true;
                    btEnterBarcode.IsEnabled = true;
                }
            }
            else if (spCommoditySearch.Children.Contains(ucPayment))
            {
                //if (ucPayment.Data != UCPayment.PayMethod.Cash)
                //{
                    //PrintOrder();
                    spCommoditySearch.Children.Clear();
                    ucBarcode = new UCBarcodeInput();
                    spCommoditySearch.Children.Add(ucBarcode);
                
                    ucPayment.DataAvailable -= new EventHandler(child_DataAvailable);
                    ucBarcode.DataAvailable += new EventHandler(child_DataAvailable);
                    btWeight.IsEnabled = true;
                    btEnterBarcode.IsEnabled = false;
                    btTerminateAndPay.IsEnabled = false;
                commoditiesList.Clear();
                CalcSubtotal();
                lvCommodities.Items.Refresh();

                
            }
        }*/
        private void btEnterBarcode_Click(object sender, RoutedEventArgs e)
        {
            
            spCommoditySearch.Children.Clear();
            ucBarcode.MainWindow = this;
            spCommoditySearch.Children.Add(ucBarcode);
            //ucBarcode.DataAvailable += new EventHandler(child_DataAvailable);

        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;

            var c = btn.DataContext as EasyModel.business.OrderItem;

            //MessageBox.Show(c.Name);

            commoditiesList.Remove(c);
            if (commoditiesList == null)
            {
                btTerminateAndPay.IsEnabled = false;
            }
            CalcSubtotal();
            lvCommodities.Items.Refresh();

        }
        public void PrintOrder(UCPayment.PayMethod payMethod)
        {
            Order order = new Order { OrderTime = DateTime.Now, Total = total };
            /*EasySaleDataFactory.Single.Database.OrderOperator.Add(order);
           
            foreach (var oi in commoditiesList)
            {
                oi.OrderID = order.ID;
                EasySaleDataFactory.Single.Database.OrderItemOperator.Add(oi);
            }*/
            EasySaleDataFactory.Single.Database.OrderOperator.Add(order, commoditiesList);
            foreach (var oi in commoditiesList)
            {
                Commodity comm = EasySaleDataFactory.Single.Database.CommodityOperator.Find(oi.CommID);
                // Normally this will not happen except that a wrong weight or quantity be entered
                if (comm.Quantity < oi.ItemCount)
                {
                    MessageBox.Show(this, "Inventory insufficient", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                comm.Quantity -= oi.ItemCount;
                EasySaleDataFactory.Single.Database.CommodityOperator.Update(comm);
            }
            string orderStr = "";
            orderStr += "Grocery Name\n";
            orderStr += "Grocery Address and Telephone\n";
            orderStr += "-----------------------------------\n";
            //orderStr += "MemberID: " + lblMemberId.Content + "\n";//memberId: choose from combobox instead of from scanner; judge if current customer is a member
            orderStr += "-----------------------------------\n";
            //            orderStr += string.Format("{0,10}{1,20}{2:0.00}{3}", "CommID", "CommName", "Subtotal","type");
            foreach (var oi in commoditiesList)
            {
                if (oi.ItemCount != 1)
                {
                    orderStr += string.Format("{0} @ {1}\n", (oi.ItemCount + "").Contains('.') ? (oi.ItemCount + "lb") : oi.ItemCount + "", oi.UnitPrice);
                }
                orderStr += string.Format("{0,10} {1,-20}{2:0.00} {3}\n", oi.CommID, oi.CommName, oi.SubTotal, oi.GST == 0 ? "" : "FP");
            }
            orderStr += string.Format("{0,18}{1,17:0.00}\n", "SUBTOTAL", subtotal);
            orderStr += string.Format("{0,18}{1,17:0.00}\n", "GST", gst);
            orderStr += string.Format("{0,18}{1,17:0.00}\n", "QST", qst);
            orderStr += string.Format("{0,18}{1,17:0.00}\n", "TOTAL", total);
            orderStr += "-----------------------------------\n";
            orderStr += "Order number:" + "111111" + "\n";//get last generated orderId from database
            orderStr += DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\n";
            orderStr += "Payment method:" + payMethod;
            // add payment method and amount
            try
            {
                File.WriteAllText(FileName, ""); // clear previous content, as the records were already read to AirportsList
                File.AppendAllText(FileName, orderStr);

            }
            catch (IOException ex)
            {
                Console.WriteLine("Failed to open file");
            }
            catch (SystemException ex)
            {
                Console.WriteLine("Failed to open file");
            }
        }
        private void btPrintOrder_Click(object sender, RoutedEventArgs e)
        {
            spCommoditySearch.Children.Clear();
            ucPayment.MainWindow = this;
            spCommoditySearch.Children.Add(ucPayment);

            //ucPayment.DataAvailable += new EventHandler(child_DataAvailable);

        }
        public void AddOrderItem(OrderItem item)
        {
            bool itemExists = false;
            foreach(var i in commoditiesList)
            {
                if (i.CommID == item.CommID)
                {
                    i.ItemCount += item.ItemCount;
                    i.SubTotal = i.ItemCount * i.UnitPrice;
                    itemExists = true;
                }
            }
            if (itemExists == false)
            {
                commoditiesList.Add(item);
            }
            btTerminateAndPay.IsEnabled = true;
            CalcSubtotal();
            lvCommodities.Items.Refresh();
        }

        public void AddUCPayInCash()
        {
            spCommoditySearch.Children.Clear();
            ucPayInCash.MainWindow = this;
            ucPayInCash.Total = total;
            ucPayInCash.lblTotal.Content = total;
            spCommoditySearch.Children.Add(ucPayInCash);
        }
        public void ClearUC()
        {
            spCommoditySearch.Children.Clear();
            ucBarcode.MainWindow = this;
            spCommoditySearch.Children.Add(ucBarcode);
        }

        public void ClearList()
        {
            commoditiesList.Clear();
            CalcSubtotal();
            btTerminateAndPay.IsEnabled = false;
            lvCommodities.Items.Refresh();
        }
    }

}
