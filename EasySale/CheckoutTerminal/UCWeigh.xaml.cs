﻿using EasyModel.business;
using EasySaleDataProvider;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckoutTerminal
{
    /// <summary>
    /// Interaction logic for UCWeigh.xaml
    /// </summary>
    public partial class UCWeigh : UserControl
    {
        public UCWeigh()
        {
            InitializeComponent();
            //List<BitmapImage> imgs = new List<BitmapImage>();
            int i = 0;
            foreach (UIElement ele in gridFresh.Children)
            {
                if (ele.GetType() == typeof(Button))
                {
                    Button bt = (Button)ele;
                    List<Commodity> commsList = EasySaleDataFactory.Single.Database.CommodityOperator.FindByName(bt.Name);
                    // those who calculated by unity shouldn't appear here
                    Commodity comm = (from c in commsList where c.Unit == CommodityUnit.lb || c.Unit == CommodityUnit.kg select c).FirstOrDefault();
                    var image = new BitmapImage();
                    if (comm != null)
                    {
                        byte[] picture = GetPicture(comm.ID);
                        if (picture == null || picture.Length == 0)
                        { }else
                        {
                            using (var mem = new MemoryStream(picture))
                            {
                                mem.Position = 0;
                                image.BeginInit();
                                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                                image.CacheOption = BitmapCacheOption.OnLoad;
                                image.UriSource = null;
                                image.StreamSource = mem;
                                image.EndInit();
                            }
                            //imgs.Add(image);
                            bt.Background = new ImageBrush
                            {
                                ImageSource = image
                            };

                        }
                    }
                }
            }
            IsControlsVisible(false);
        }
        public void IsControlsVisible (bool isVisible)
        {
            if (isVisible == true)
            {
                lblSubtotal.Content = "$0.00";
                tbWeight.Text = "";
                comboComm.Visibility = Visibility.Visible;
                tbWeight.Visibility = Visibility.Visible;
                lblSubtotal.Visibility = Visibility.Visible;
                btAdd.Visibility = Visibility.Visible;
                lbl1.Visibility = Visibility.Visible;
                lbl2.Visibility = Visibility.Visible;
            } else
            {
                comboComm.Visibility = Visibility.Hidden;
                tbWeight.Visibility = Visibility.Hidden;
                lblSubtotal.Visibility = Visibility.Hidden;
                btAdd.Visibility = Visibility.Hidden;
                lbl1.Visibility = Visibility.Hidden;
                lbl2.Visibility = Visibility.Hidden;
            }
        }
        /// Data to transfer into / out of form
        /// </summary>
        string commName;
        EasyModel.business.OrderItem oi;
        /*public EasyModel.business.OrderItem Data
        {
            // string barcode = from commodity in commoditydatabase where Name = commName select commodity.Barcode
            // CommCode
            // price
            // subtotal: lblSubtotal.Content = price * weight
            
            get {
                
                return oi;
            }
            //set { tbWeight.Text = value; }
        }*/

        /// <summary>
        /// Event to indicate new data is available
        /// </summary>
        //public event EventHandler DataAvailable;
        /// <summary>
        /// Called to signal to subscribers that new data is available
        /// </summary>
        /// <param name="e"></param>
        /*protected virtual void OnDataAvailable(EventArgs e)
        {
            EventHandler eh = DataAvailable;
            if (eh != null)
            {
                eh(this, e);
            }
        }*/
        public MainWindow MainWindow
        {
            get; set;
        }
        EasyModel.business.Commodity comm;
        private void butTransfer_Click(object sender, EventArgs e)
        {
            double weight;
            if (!double.TryParse(tbWeight.Text, out weight))
            {
                MessageBox.Show("Wrong weight");
                return;
            }
            
            // get information from commoditydatabase, instantiate new orderItem and return to mainwindow when Add clicked
            // from commodity in commoditydatabase where Name = commName select commodity.Barcode
            //double gst, qst;
            if (comm != null)
            {
                oi = new EasyModel.business.OrderItem { CommID = comm.ID, CommName = comm.Name, ItemCount = weight, UnitPrice = comm.Price, SubTotal = comm.Price * weight, GST = 0, PST = 0 };
                IsControlsVisible(false);
                tbWeight.Text = "";
                lblSubtotal.Content = "$0.00";
                if (comm.Quantity < oi.ItemCount)
                {
                    MessageBox.Show("Inventory insufficient", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                MainWindow.AddOrderItem(oi);
                // lblName.Content = "";
                // lblPrice.Content = "";
            } else
            {
                MessageBox.Show("Commodity doesn't exist");
            }
            
            //oi = new EasyModel.business.OrderItem { CommID = 111113, CommName = commName, ItemCount = weight, UnitPrice = 0.99, SubTotal = weight*0.99 };
            
            //OnDataAvailable(null);
        }
        private void btFinish_Click(object sender, EventArgs e)
        {
            // oi = null;
            // OnDataAvailable(null);
            MainWindow.ClearUC();

        }

        private void btComm_click(object sender, RoutedEventArgs e)
        {
            lblSubtotal.Content = "";
            tbWeight.Text = "";
            comm = null;
            Button button = (Button)sender;
            commName = button.Name.ToString();

            try
            {
                //comm = EasySaleDataFactory.Single.Database.CommodityOperator.FindByName(commName).First();
                List<Commodity> commsList = EasySaleDataFactory.Single.Database.CommodityOperator.FindByName(commName);
                // those who calculated by unity shouldn't appear here
                commsList = (from c in commsList where c.Unit == CommodityUnit.lb || c.Unit == CommodityUnit.kg select c).ToList();
                if (commsList != null)
                {
                    IsControlsVisible(true);
                    comboComm.ItemsSource = commsList;
                    comboComm.SelectedItem = commsList.First();
                } else
                {
                    //comm = null;
                    IsControlsVisible(false);
                    MessageBox.Show("Commodity doesn't exist");
                    
                }
                //lblName.Content = comm.Name;
                //lblPrice.Content = comm.Price;
            }
            catch (InvalidOperationException ex)
            {
                //comm = null;
                IsControlsVisible(false);
                MessageBox.Show("Commodity doesn't exist");
            }
        }

       
        private void tbWeight_TextChanged(object sender, TextChangedEventArgs e)
        {
            double weight;
            if (tbWeight.Text != "" && comm != null)
            {
                if (!double.TryParse(tbWeight.Text, out weight))
                {
                    MessageBox.Show("Wrong weight");
                    return;
                }
                
                lblSubtotal.Content = "$" + comm.Price * weight;
            }
        }

        private void comboComm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            comm = (Commodity)comboComm.SelectedItem;
        }
        private byte[] GetPicture(int commID)
        { 
            string path=AppDomain.CurrentDomain.BaseDirectory+@"\commodityImg\";
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);

            if (File.Exists(string.Format("{0}{1}.jpg", path, commID)))
            {
                //load image from file
                return null;
            }
            else
            {
                byte[] picture = EasySaleDataFactory.Single.Database.CommodityOperator.GetPicture(commID);
                //save picture to file
                
                return picture;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
