﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckoutTerminal
{
    public interface IOperatePage
    {
        void Open();
        void Close();
    }
}
