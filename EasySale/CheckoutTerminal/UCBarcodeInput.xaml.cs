﻿using EasySaleDataProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CheckoutTerminal
{
    /// <summary>
    /// Interaction logic for UCBarcodeInput.xaml
    /// </summary>
    public partial class UCBarcodeInput : UserControl, IOperatePage
    {
        public UCBarcodeInput()
        {
            InitializeComponent();
            btAdd.Visibility = Visibility.Hidden;
            lblSearchResult.Visibility = Visibility.Hidden;
        }
        public MainWindow MainWindow
        {
            get;set;
        }
        EasyModel.business.OrderItem oi;
        EasyModel.business.Commodity comm;
        private void btSearch_Click(object sender, RoutedEventArgs e)
        {

            EasyModel.business.CommodityType ct = null;
            lblSearchResult.Visibility = Visibility.Visible;
            tbSearch.Text = "";
            lblPrice.Content = "";
            lblName.Content = "";
            btAdd.Visibility = Visibility.Hidden;
            // get comm

            try
            {
                comm = EasySaleDataFactory.Single.Database.CommodityOperator.FindByBarcode(tbSearch.Text);
            } catch (InvalidOperationException ex)
            {
                comm = null;
            }
            if (comm != null)
            {
                ct = EasySaleDataFactory.Single.Database.CommodityTypeOperator.Find(comm.CommType);
                btAdd.Visibility = Visibility.Visible;
            }
            //tbSearch.Text = "";
            // get information from commoditydatabase, instantiate new orderItem and return to mainwindow when Add clicked
            // from commodity in commoditydatabase where Name = commName select commodity.Barcode
            double gst, qst;
            if (comm != null)
            {
                lblSearchResult.Content = "Commodity found:";
                lblName.Content = comm.Name;
                lblPrice.Content = "UnitPrice: $" + comm.Price;
                tbSearch.Text = "";
                btAdd.Visibility = Visibility.Visible;
                gst = ct.GST;
                qst = ct.PST;
                oi = new EasyModel.business.OrderItem { CommID = comm.ID, CommName = comm.Name, ItemCount = 1, UnitPrice = comm.Price, SubTotal = comm.Price, GST = gst, PST = qst };
                Console.WriteLine("found commodity type{0}", ct.TypeCode);
            } else // msgbox 'haven't find', return
            {
                lblSearchResult.Content = "Commodity not found";
                // for test
                /*gst = 0.05;
                qst = 0.09975;
                Console.WriteLine("haven't found comm");*/
            }
            // for test
            /*oi = new EasyModel.business.OrderItem { CommID = 111113, CommName = tbSearch.Text, ItemCount = 1, UnitPrice = 0.99, SubTotal = 0.99, GST = gst, PST = qst};
            lblName.Content = oi.CommName;
            lblPrice.Content = "UnitPrice: $" + oi.UnitPrice;
            tbSearch.Text = "";
            btAdd.Visibility = Visibility.Visible;// delete later
            lblSearchResult.Content = "Commodity found:";*/

        }

        /*public EasyModel.business.OrderItem Data
        {
            // string barcode = from commodity in commoditydatabase where Name = commName select commodity.Barcode
            // CommCode
            // price
            // subtotal: lblSubtotal.Content = price * weight

            get
            {
                
                return oi;
            }
            //set { tbWeight.Text = value; }
        }*/

        /// <summary>
        /// Event to indicate new data is available
        /// </summary>
        //public event EventHandler DataAvailable;
        
        /*protected virtual void OnDataAvailable(EventArgs e)
        {
            EventHandler eh = DataAvailable;
            if (eh != null)
            {
                eh(this, e);
            }
        }*/

        
        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            if (oi == null)
            {
                MessageBox.Show("Please choose a Commodity.");
                return;
            }
            if (comm.Quantity < oi.ItemCount)
            {
                MessageBox.Show( "Inventory insufficient", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            MainWindow.AddOrderItem(oi);
            lblPrice.Content = "";
            lblName.Content = "";
            btAdd.Visibility = Visibility.Hidden;
            lblSearchResult.Visibility = Visibility.Hidden;
            //OnDataAvailable(null);
        }

        private void btFinish_Click(object sender, RoutedEventArgs e)
        {
            /*oi = null;
            OnDataAvailable(null);*/
            MainWindow.ClearUC();
            lblSearchResult.Visibility = Visibility.Hidden;
        }

        /*private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            if (oi == null)
            {
                MessageBox.Show("Please choose a Commodity.");
                return; 
            }
            OnDataAvailable(null);
        }*/

        public void Open()
        {
            //+=
        }

        public void Close()
        {
            //-=
        }
    }
}
