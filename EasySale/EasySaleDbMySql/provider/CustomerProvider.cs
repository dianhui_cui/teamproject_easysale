﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
using EasyModel.database;
using EasySaleDbMySql.db;
namespace EasySaleDbMySql.provider
{
    public class CustomerProvider : ProviderBase,ICustomer
    {
        public CustomerProvider(EasySaleDb easysaleEntities) : base(easysaleEntities)
        { }

        public void Add(Customer customer)
        {
            DbCustomer dbCustomer = null;
            DataCopier.ToDbObject(customer, ref dbCustomer);
            DbContext.Customers.Add(dbCustomer);
            DbContext.SaveChanges();
            customer.ID = dbCustomer.ID;
        }

        public Customer Find(int id)
        {
            return DataCopier.FromDbObject(DbContext.Customers.Find(id));
        }

        public Customer Find(string MemberCardNo)
        {
            return DataCopier.FromDbObject(DbContext.Customers.First(customer=>customer.MemberCardNo==MemberCardNo));
        }

        public List<Customer> FindAll()
        {
            return DataCopier.FromDbObject(DbContext.Customers.ToList());
        }

        public void Update(Customer customer)
        {
            DbCustomer dbCustomer = DbContext.Customers.Find(customer.ID);
            DataCopier.ToDbObject(customer, ref dbCustomer);
            DbContext.SaveChanges();
        }
    }
}
