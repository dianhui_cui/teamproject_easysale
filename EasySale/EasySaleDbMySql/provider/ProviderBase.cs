﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasySaleDbMySql.db;

namespace EasySaleDbMySql.provider
{
    public abstract class ProviderBase
    {
        //private EasysaleEntities _easysaleEntities;
        private EasySaleDb _easySaleDb;
        public ProviderBase(EasySaleDb easySaleDb)
        {
            _easySaleDb = easySaleDb;
        }
        public EasySaleDb DbContext
        {
            get { return _easySaleDb; }
        }
    }
}
