﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
using EasyModel.database;
using EasySaleDbMySql.db;
namespace EasySaleDbMySql.provider
{
    public class OrderProvider : ProviderBase,IOrder
    {
        public OrderProvider(EasySaleDb easysaleEntities) : base(easysaleEntities)
        { }

        public void Add(Order order)
        {
            DbOrder dbOrder = null;
            DataCopier.ToDbObject(order, ref dbOrder);
            DbContext.Orders.Add(dbOrder);
            DbContext.SaveChanges();
            order.ID = dbOrder.ID;
        }

        public void Add(Order order, List<OrderItem> items)
        {
            DbOrder dbOrder = null;
            DataCopier.ToDbObject(order, ref dbOrder);
            foreach (var item in items)
            {
                DbOrderItem dbOrderItem = null;
                DataCopier.ToDbObject(item, ref dbOrderItem);
                dbOrder.OrderItems.Add(dbOrderItem);
            }
            DbContext.Orders.Add(dbOrder);
            DbContext.SaveChanges();
            order.ID = dbOrder.ID;
        }

        public Order Find(int id)
        {
            return DataCopier.FromDbObject( DbContext.Orders.Find(id));
        }

        public List<Order> FindByCustomer(Customer customer)
        {
            return DataCopier.FromDbObject(DbContext.Customers.Find(customer.ID).Orders.ToList());
        }

        public List<Order> FindByCustomerID(int custID)
        {
             return DataCopier.FromDbObject(DbContext.Customers.Find(custID).Orders.ToList());
        }

        public List<Order> FindByTime(DateTime start, DateTime end)
        {
            return DataCopier.FromDbObject((from order in DbContext.Orders where order.OrderTime>=start &&order.OrderTime<=end select order).ToList());
        }
    }
}
