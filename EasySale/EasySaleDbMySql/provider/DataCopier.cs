﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
using EasySaleDbMySql.db;

namespace EasySaleDbMySql.provider
{
    public class DataCopier
    {
        public static EasySaleDb DBCONTEXT;

        public static void ToDbObject(Commodity commodity,ref DbCommodity dbCommodity)
        {
            if (dbCommodity == null)
            {
                dbCommodity = new DbCommodity();
                if (commodity.MemberPrice > 0)
                {
                    dbCommodity.MemberPrice = commodity.MemberPrice;
                }
                dbCommodity.Name = commodity.Name;
                dbCommodity.Picture = commodity.Picture;
                dbCommodity.Price = commodity.Price;
                dbCommodity.Quantity = commodity.Quantity;
                dbCommodity.Unit = commodity.Unit;
                dbCommodity.CommodityType = DBCONTEXT.CommodityTypes.Find(commodity.CommType);
                dbCommodity.CommType = commodity.CommType;
            }
            else
            {
                if (commodity.MemberPrice > 0)
                {
                    if (dbCommodity.MemberPrice != commodity.MemberPrice) dbCommodity.MemberPrice = commodity.MemberPrice;
                }
                if (dbCommodity.Name != commodity.Name) dbCommodity.Name = commodity.Name;
                if (dbCommodity.Picture != commodity.Picture) dbCommodity.Picture = commodity.Picture;
                if (dbCommodity.Price != commodity.Price) dbCommodity.Price = commodity.Price;
                if (dbCommodity.Quantity != commodity.Quantity) dbCommodity.Quantity = commodity.Quantity;
                if (dbCommodity.Unit != commodity.Unit) dbCommodity.Unit = commodity.Unit;
                if (dbCommodity.CommType != commodity.CommType)
                {
                    dbCommodity.CommodityType = DBCONTEXT.CommodityTypes.Find(commodity.CommType);
                    dbCommodity.CommType = commodity.CommType;
                }
            }
         
        }
        public static void ToDbObject(CommodityType commodityType, ref DbCommodityType dbCommodityType)
        {
            if (dbCommodityType == null)
            {
                dbCommodityType = new DbCommodityType();
                dbCommodityType.TypeCode = commodityType.TypeCode;
                dbCommodityType.TypeName = commodityType.TypeName;
                if (commodityType.ParentCode > 0)
                    dbCommodityType.ParentType = DBCONTEXT.CommodityTypes.Find(commodityType.ParentCode);

            }
            else
            {
                if (dbCommodityType.TypeName != commodityType.TypeName) dbCommodityType.TypeName = commodityType.TypeName;
            }


        }
        public static void ToDbObject(Supplier supplier,ref DbSupplier dbSupplier)
        {
            if (dbSupplier == null)
            {
                dbSupplier = new DbSupplier();
                dbSupplier.Address = supplier.Address;
                dbSupplier.City = supplier.City;
                dbSupplier.Company = supplier.Company;
                dbSupplier.ContactName = supplier.ContactName;
                dbSupplier.Country = supplier.Country;
                dbSupplier.EMail = supplier.Email;
                dbSupplier.Memo = supplier.Memo;
                dbSupplier.PostalCode = supplier.PostalCode;
                dbSupplier.ProductTypes = supplier.ProductTypes;
                dbSupplier.Province = supplier.Province;
                dbSupplier.Telephone = supplier.Telephone;
            }
            else {
                if (dbSupplier.Address != supplier.Address) dbSupplier.Address = supplier.Address;
                if (dbSupplier.City != supplier.City) dbSupplier.City = supplier.City;
                if (dbSupplier.Company != supplier.Company) dbSupplier.Company = supplier.Company;
                if (dbSupplier.ContactName != supplier.ContactName) dbSupplier.ContactName = supplier.ContactName;
                if (dbSupplier.Country != supplier.Country) dbSupplier.Country = supplier.Country;
                if (dbSupplier.EMail != supplier.Email) dbSupplier.EMail = supplier.Email;
                if (dbSupplier.Memo != supplier.Memo) dbSupplier.Memo = supplier.Memo;
                if (dbSupplier.PostalCode != supplier.PostalCode) dbSupplier.PostalCode = supplier.PostalCode;
                if (dbSupplier.ProductTypes != supplier.ProductTypes) dbSupplier.ProductTypes = supplier.ProductTypes;
                if (dbSupplier.Province != supplier.Province) dbSupplier.Province = supplier.Province;
                if (dbSupplier.Telephone != supplier.Telephone) dbSupplier.Telephone = supplier.Telephone;
            }
           
        }
        public static void ToDbObject(Purchase purchase,ref DbPurchase dbPurchase)
        {
            if (dbPurchase == null)
            {
                dbPurchase = new DbPurchase();
                dbPurchase.CommID = purchase.CommID;
                dbPurchase.CommNumber = purchase.CommNumber;
                dbPurchase.Commodity = DBCONTEXT.Commodities.First(comm => comm.ID == purchase.CommID);
                dbPurchase.OrderDate = purchase.OrderDate;
                dbPurchase.PurchaseDate = purchase.IsPurchased ? purchase.PurchaseDate : null;
                dbPurchase.Total = purchase.Total;
                dbPurchase.Supplier = DBCONTEXT.Suppliers.First(supplier => supplier.ID == purchase.ID);
                dbPurchase.SupplierID = purchase.SupplierID;
            }
            else
            {
                if (dbPurchase.CommID != purchase.CommID) dbPurchase.CommID = purchase.CommID;
                if (dbPurchase.CommNumber != purchase.CommNumber) dbPurchase.CommNumber = purchase.CommNumber;
                if (dbPurchase.CommID != purchase.CommID) dbPurchase.Commodity = DBCONTEXT.Commodities.First(comm => comm.ID == purchase.CommID);
                if (dbPurchase.OrderDate != purchase.OrderDate) dbPurchase.OrderDate = purchase.OrderDate;
                if (dbPurchase.PurchaseDate != purchase.PurchaseDate) dbPurchase.PurchaseDate = purchase.IsPurchased ? purchase.PurchaseDate : null;
                if (dbPurchase.Total != purchase.Total) dbPurchase.Total = purchase.Total;
                if (dbPurchase.SupplierID != purchase.SupplierID)
                {
                    dbPurchase.Supplier = DBCONTEXT.Suppliers.First(supplier => supplier.ID == purchase.ID);
                    dbPurchase.SupplierID = purchase.SupplierID;
                }
            }

        }
        public static void ToDbObject(Customer customer,ref DbCustomer dbCustomer)
        {
            if (dbCustomer == null)
            {
                dbCustomer = new DbCustomer();
                dbCustomer.Address = customer.Address;
                dbCustomer.Balance = customer.Balance;
                dbCustomer.City = customer.City;
                dbCustomer.Country = customer.Country;
                dbCustomer.Email = customer.Email;
                dbCustomer.FirstName = customer.FirstName;
                dbCustomer.LastName = customer.LastName;
                dbCustomer.MemberCardNo = customer.MemberCardNo;
                dbCustomer.PostalCode = customer.PostalCode;
                dbCustomer.Province = customer.Province;
                dbCustomer.Telephone = customer.Telephone;
            }
            else {

                if (dbCustomer.Address != customer.Address) dbCustomer.Address = customer.Address;
                if (dbCustomer.Balance != customer.Balance) dbCustomer.Balance = customer.Balance;
                if (dbCustomer.City != customer.City) dbCustomer.City = customer.City;
                if (dbCustomer.Country != customer.Country) dbCustomer.Country = customer.Country;
                if (dbCustomer.Email != customer.Email) dbCustomer.Email = customer.Email;
                if (dbCustomer.FirstName != customer.FirstName) dbCustomer.FirstName = customer.FirstName;
                if (dbCustomer.LastName != customer.LastName) dbCustomer.LastName = customer.LastName;
                if (dbCustomer.MemberCardNo != customer.MemberCardNo) dbCustomer.MemberCardNo = customer.MemberCardNo;
                if (dbCustomer.PostalCode != customer.PostalCode) dbCustomer.PostalCode = customer.PostalCode;
                if (dbCustomer.Province != customer.Province) dbCustomer.Province = customer.Province;
                if (dbCustomer.Telephone != customer.Telephone) dbCustomer.Telephone = customer.Telephone;
            }
           
           
        }
        public static void ToDbObject(Order order,ref DbOrder dbOrder)
        {
            if (dbOrder == null)
            {
                dbOrder = new DbOrder();
                if (order.CustomerID > 0)
                {
                    dbOrder.CustomerID = order.CustomerID;
                    dbOrder.Customer = DBCONTEXT.Customers.First(customer => customer.ID == order.CustomerID);
                }
                dbOrder.OrderTime = order.OrderTime;
                dbOrder.Total = order.Total;

            }
            else 
            {

                if (dbOrder.OrderTime != order.OrderTime) dbOrder.OrderTime = order.OrderTime;
                if (dbOrder.Total != order.Total) dbOrder.Total = order.Total;
            }
        }
        public static void ToDbObject(OrderItem orderItem,ref DbOrderItem dbOrderItem)
        {
            if (dbOrderItem == null)
            {
                dbOrderItem = new DbOrderItem();
                dbOrderItem.CommID = orderItem.CommID;
                dbOrderItem.Commodity = DBCONTEXT.Commodities.First(comm => comm.ID == orderItem.CommID);
                dbOrderItem.ItemCount = orderItem.ItemCount;
                dbOrderItem.OrderID = orderItem.OrderID;
                dbOrderItem.Order = DBCONTEXT.Orders.First(order => order.ID == orderItem.ID);
                dbOrderItem.SubTotal = orderItem.SubTotal;
                dbOrderItem.UnitPrice = orderItem.UnitPrice;
                dbOrderItem.GST = orderItem.GST;
                dbOrderItem.PST = orderItem.PST;
            }
            else
            {
                if (dbOrderItem.ItemCount != orderItem.ItemCount) dbOrderItem.ItemCount = orderItem.ItemCount;
                if (dbOrderItem.SubTotal != orderItem.SubTotal) dbOrderItem.SubTotal = orderItem.SubTotal;
                if (dbOrderItem.UnitPrice != orderItem.UnitPrice) dbOrderItem.UnitPrice = orderItem.UnitPrice;
                if (dbOrderItem.GST != orderItem.GST) dbOrderItem.GST = orderItem.GST;
                if (dbOrderItem.PST != orderItem.PST) dbOrderItem.PST = orderItem.PST;

            }
        }
        public static Commodity FromDbObject(DbCommodity dbCommodity)
        {
            Commodity commodity = new Commodity()
            {
                ID = dbCommodity.ID,
                Barcode = dbCommodity.Barcode,
                CommCode = dbCommodity.CommCode == null ? 0 : dbCommodity.CommCode.Value,
                CommType = dbCommodity.CommodityType.TypeCode,
                CommTypeName = dbCommodity.CommodityType.TypeName,
                MemberPrice = dbCommodity.MemberPrice == null ? 0 : dbCommodity.MemberPrice.Value,
                Name = dbCommodity.Name,
                Picture = dbCommodity.Picture,
                Price=dbCommodity.Price,
                Quantity=dbCommodity.Quantity,
                Unit=dbCommodity.Unit
                
            };
            return commodity;
        }
        public static CommodityType FromDbObject(DbCommodityType dbCommodityType)
        {
            CommodityType commodityType = new CommodityType()
            {
                TypeCode = dbCommodityType.TypeCode,
                TypeName=dbCommodityType.TypeName,
                ParentCode=dbCommodityType.ParentType==null?0:dbCommodityType.ParentType.TypeCode,
                GST=dbCommodityType.GST,
                PST=dbCommodityType.PST
                
            };
            return commodityType;
        }
        public static Supplier FromDbObject(DbSupplier dbSupplier)
        {
            Supplier supplier = new Supplier()
            { 
                ID=dbSupplier.ID,
                Address=dbSupplier.Address,
                City=dbSupplier.City,
                Company=dbSupplier.Company,
                ContactName=dbSupplier.ContactName,
                Country=dbSupplier.Country,
                Email=dbSupplier.EMail,
                Memo=dbSupplier.Memo,
                PostalCode=dbSupplier.PostalCode,
                ProductTypes=dbSupplier.ProductTypes,
                Province=dbSupplier.Province,
                Telephone=dbSupplier.Telephone
            };
            return supplier;
        }
        public static Purchase FromDbObject(DbPurchase dbPurchase)
        {
            Purchase purchase = new Purchase()
            {
                ID = dbPurchase.ID,
                CommID = dbPurchase.CommID,
                CommName = dbPurchase.Commodity.Name,
                CommNumber = dbPurchase.CommNumber,
                IsPurchased = dbPurchase.PurchaseDate == null ? false : true,
                OrderDate=dbPurchase.OrderDate,
                Price=dbPurchase.Price,
                PurchaseDate=dbPurchase.PurchaseDate,
                SupplierID=dbPurchase.SupplierID,
                SupplierName=dbPurchase.Supplier.Company
            };
            return purchase;
        }
        public static Customer FromDbObject(DbCustomer dbCustomer)
        {
            Customer customer = new Customer()
            { 
                ID=dbCustomer.ID,
                Address=dbCustomer.Address,
                Balance=dbCustomer.Balance,
                City=dbCustomer.City,
                Country=dbCustomer.Country,
                Email=dbCustomer.Email,
                FirstName=dbCustomer.FirstName,
                LastName=dbCustomer.LastName,
                MemberCardNo=dbCustomer.MemberCardNo,
                PostalCode=dbCustomer.PostalCode,
                Province=dbCustomer.Province,
                Telephone=dbCustomer.Telephone
            };
            return customer;
        }
        public static Order FromDbObject(DbOrder dbOrder)
        {
            Order order = new Order()
            { 
                ID=dbOrder.ID,
                CustomerFullName=string.Format("{0} {1}", dbOrder.Customer.FirstName,dbOrder.Customer.LastName),
                CustomerID=dbOrder.CustomerID==null?0:dbOrder.CustomerID.Value,
                OrderTime=dbOrder.OrderTime,
                Total=dbOrder.Total
            };
            return order;
        }
        public static OrderItem FromDbObject(DbOrderItem dbOrderItem)
        {
            OrderItem orderItem = new OrderItem()
            { 
                ID=dbOrderItem.ID,
                OrderID=dbOrderItem.OrderID,
                CommID=dbOrderItem.CommID,
                CommName=dbOrderItem.Commodity.Name,
                ItemCount=dbOrderItem.ItemCount,
                SubTotal=dbOrderItem.SubTotal,
                UnitPrice=dbOrderItem.UnitPrice,
                GST=dbOrderItem.GST,
                PST=dbOrderItem.PST

            };
            return orderItem;
        }
        public static List<Commodity> FromDbObject(List<DbCommodity> dbCommodities)
        {
            return (from comm in dbCommodities select FromDbObject(comm)).ToList();
        }
        public static List<CommodityType> FromDbObject(List<DbCommodityType> dbCommodityTypes)
        {
            return (from type in dbCommodityTypes select FromDbObject(type)).ToList();
        }
        public static List<Supplier> FromDbObject(List<DbSupplier> dbSuppliers)
        {
            return (from supplier in dbSuppliers select FromDbObject(supplier)).ToList();
        }
        public static List<Purchase> FromDbObject(List<DbPurchase> dbPurchases)
        {
            return (from purchase in dbPurchases select FromDbObject(purchase)).ToList();
        }
        public static List<Customer> FromDbObject(List<DbCustomer> dbCustomers)
        {
            return (from customer in dbCustomers select FromDbObject(customer)).ToList();
        }
        public static List<Order> FromDbObject(List<DbOrder> dbOrders)
        {
            return (from order in dbOrders select FromDbObject(order)).ToList();
        }
        public static List<OrderItem> FromDbObject(List<DbOrderItem> dbOrderItems)
        {
            return (from orderItem in dbOrderItems select FromDbObject(orderItem)).ToList();
        }
    }
}
