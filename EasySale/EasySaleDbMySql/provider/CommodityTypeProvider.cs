﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
using EasyModel.database;
using EasySaleDbMySql.db;
namespace EasySaleDbMySql.provider
{
    public class CommodityTypeProvider : ProviderBase,ICommodityType
    {
        public CommodityTypeProvider(EasySaleDb easysaleEntities) : base(easysaleEntities)
        { }

        public void Add(CommodityType commodityType)
        {
            DbCommodityType dbCommodityType = null;
            DataCopier.ToDbObject(commodityType, ref dbCommodityType);
            DbContext.CommodityTypes.Add(dbCommodityType);
            DbContext.SaveChanges();
        }

        public CommodityType Find(int code)
        {
            return DataCopier.FromDbObject(DbContext.CommodityTypes.Find(code));
        }

        public List<CommodityType> FindByParent(int parentCode)
        {
            return DataCopier.FromDbObject(DbContext.CommodityTypes.Find(parentCode).ChildrenTypes.ToList());
        }

        public void Update(CommodityType commodityType)
        {
            DbCommodityType dbCommodityType = DbContext.CommodityTypes.Find(commodityType.TypeCode);
            DataCopier.ToDbObject(commodityType, ref dbCommodityType);
            DbContext.SaveChanges();
        }
    }
}
