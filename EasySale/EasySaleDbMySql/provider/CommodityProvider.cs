﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
using EasyModel.database;
using EasySaleDbMySql.db;
namespace EasySaleDbMySql.provider
{
    public class CommodityProvider : ProviderBase,ICommodity
    {
        public CommodityProvider(EasySaleDb easysaleEntities) : base(easysaleEntities)
        { }

        public void Add(Commodity commodity)
        {
            DbCommodity dbCommodity=null;
            DataCopier.ToDbObject(commodity,ref dbCommodity);
            DbContext.Commodities.Add(dbCommodity);
            DbContext.SaveChanges();
            commodity.ID = dbCommodity.ID;
         
        }

        public List<Commodity> Find(CommodityType type)
        {
            return DataCopier.FromDbObject(DbContext.CommodityTypes.Find(type.TypeCode).Commodities.ToList());
        }

        public Commodity Find(int id)
        {
            return DataCopier.FromDbObject(DbContext.Commodities.Find(id));
   
        }

        public List<Commodity> FindAll()
        {
            throw new NotImplementedException();
        }

        public Commodity FindByBarcode(string barcode)
        {
            return DataCopier.FromDbObject(DbContext.Commodities.First(comm=>comm.Barcode==barcode));
        }

        public Commodity FindByCommCode(int commCode)
        {
            return DataCopier.FromDbObject(DbContext.Commodities.First(comm => comm.CommCode == commCode));

        }

        public List<Commodity> FindByName(string name)
        {
            throw new NotImplementedException();
        }

        public void Update(Commodity commodity)
        {
            DbCommodity dbCommodity = DbContext.Commodities.Find(commodity.ID);
            DataCopier.ToDbObject(commodity, ref dbCommodity);
            DbContext.SaveChanges();
        }
    }
}
