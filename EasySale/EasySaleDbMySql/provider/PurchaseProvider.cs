﻿using EasyModel.business;
using EasyModel.database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasySaleDbMySql.db;
namespace EasySaleDbMySql.provider
{
    public class PurchaseProvider : ProviderBase,IPurchase
    {
        public PurchaseProvider(EasySaleDb easysaleEntities) : base(easysaleEntities)
        { }

        public void Add(Purchase purchase)
        {
            DbPurchase dbPurchase = null;
            DataCopier.ToDbObject(purchase, ref dbPurchase);
            DbContext.Purchases.Add(dbPurchase);
            DbContext.SaveChanges();
            purchase.ID = dbPurchase.ID;
        }

        public void Add(List<Purchase> purchases)
        {
            foreach (var purchase in purchases)
            {
                DbPurchase dbPurchase = null;
                DataCopier.ToDbObject(purchase, ref dbPurchase);
                DbContext.Purchases.Add(dbPurchase);
            }
            DbContext.SaveChanges();
        }

        public Purchase Find(int id)
        {
            return DataCopier.FromDbObject(DbContext.Purchases.Find(id));
        }

        public List<Purchase> FindAll(bool isCompleted)
        {
            if (isCompleted)
            {
                return DataCopier.FromDbObject((from purchase in DbContext.Purchases where purchase.PurchaseDate != null select purchase).ToList());
            }
            else
            {
                return DataCopier.FromDbObject((from purchase in DbContext.Purchases where purchase.PurchaseDate == null select purchase).ToList());
            }
        }

        public List<Purchase> findBySupplier(int supplierID)
        {
            return DataCopier.FromDbObject(DbContext.Suppliers.Find(supplierID).Purchases.ToList());
        }

        public List<Purchase> findBySupplier(int supplierID, bool isCompleted)
        {
            if (isCompleted)
            {
                return DataCopier.FromDbObject((from purchase in DbContext.Purchases where purchase.PurchaseDate != null &&purchase.SupplierID==supplierID select purchase).ToList());
            }
            else
            {
                return DataCopier.FromDbObject((from purchase in DbContext.Purchases where purchase.PurchaseDate == null && purchase.SupplierID == supplierID select purchase).ToList());
            }
        }

        public void Update(Purchase purchase)
        {
            DbPurchase dbPurchase = DbContext.Purchases.Find(purchase.ID);
            DataCopier.ToDbObject(purchase, ref dbPurchase);
            DbContext.SaveChanges();
        }
    }
}
