﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasySaleDbMySql.db
{
    //[DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class EasySaleDb:DbContext
    {
        public EasySaleDb(): base("name=EasySaleDb")
        {

        }
        public EasySaleDb(DbConnection existingConnection, bool contextOwnsConnection)
          : base(existingConnection, contextOwnsConnection)
        {

        }
        public virtual DbSet<DbCommodity> Commodities { get; set; }
        public virtual DbSet<DbCommodityType> CommodityTypes { get; set; }
        public virtual DbSet<DbCustomer> Customers { get; set; }
        public virtual DbSet<DbOrderItem> OrderItems { get; set; }
        public virtual DbSet<DbOrder> Orders { get; set; }
        public virtual DbSet<DbPurchase> Purchases { get; set; }
        public virtual DbSet<DbSupplier> Suppliers { get; set; }

    }
}
