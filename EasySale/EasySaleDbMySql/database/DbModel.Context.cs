﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EasySaleDbMySql.database
{
    using System;
    using System.Data.Common;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class EasysaleEntities : DbContext
    {
        public EasysaleEntities()
            : base("name=EasysaleEntities")
        {
        }
    	  public EasysaleEntities(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection,contextOwnsConnection)
        {
    	}
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<DbCommodity> Commodities { get; set; }
        public virtual DbSet<DbCommodityType> CommodityTypes { get; set; }
        public virtual DbSet<DbCustomer> Customers { get; set; }
        public virtual DbSet<DbOrderItem> OrderItems { get; set; }
        public virtual DbSet<DbOrder> Orders { get; set; }
        public virtual DbSet<DbPurchase> Purchases { get; set; }
        public virtual DbSet<DbSupplier> Suppliers { get; set; }
    }
}
