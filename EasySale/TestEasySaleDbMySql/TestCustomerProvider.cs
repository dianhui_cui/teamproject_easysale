﻿using EasyModel.business;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEasySaleDbMySql
{
    [TestFixture]
    public class TestCustomerProvider
    {
        [Test]
        public void AddCustomer()
        {
            Customer customer = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Find(1);
            
            if (customer == null)
            {
                customer = new Customer()
                {
                    Address="23 Rue Brunswick",
                    Balance=100,
                    City="Point-Claire",
                    Country="Canada",
                    Email="ferry.smith@gmail.com",
                    FirstName="Ferry",
                    LastName="Smith",
                    MemberCardNo="00000001",
                    PostalCode="Y7T9U8",
                    Province="Quebec",
                    Telephone="514-456-7844"
                };
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Add(customer);
                Assert.AreEqual(1, customer.ID);
                customer = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Find(1);

            }
            Assert.AreEqual("Ferry", customer.FirstName);
            Assert.AreEqual("Smith", customer.LastName);
        }
        [Test]
        public void UpdateCustomer()
        {
            Customer customer = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Find(1);
            customer.Telephone = "514-456-7888";
            EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Update(customer);
            customer = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Find(1);
            Assert.AreEqual("514-456-7888", customer.Telephone);
        }
        [Test]
        public void FindByMemberCard()
        {
            Customer customer = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Find("00000001");
            Assert.AreEqual("Ferry", customer.FirstName);
            Assert.AreEqual("Smith", customer.LastName);
        }
        [Test]
        public void FindReturnsNull()
        {
            Customer customer = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Find(10000);
            Assert.AreEqual(null, customer);
            customer = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Find("99999999");
            Assert.AreEqual(null, customer);

        }
    }
}
