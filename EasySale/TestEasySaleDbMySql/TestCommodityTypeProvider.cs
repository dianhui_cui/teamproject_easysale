﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
using EasySaleDataProvider;

namespace TestEasySaleDbMySql
{
    [TestFixture]
    public class TestCommodityTypeProvider
    {
        [Test]
        public void AddCommodityType()
        {
            CommodityType fruit = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.Find(11);
            if (null == fruit)
            {
                CommodityType type = new CommodityType()
                {
                    TypeCode = 11,
                    TypeName="Fruit",
                    GST=0,
                    PST=0,
                    ParentCode=1
                };
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.Add(type);
            }
            fruit = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.Find(11);
            Assert.AreEqual("Fruit", fruit.TypeName);
        }
        [Test]
        public void FindByParent()
        {

            List<CommodityType> types = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(1);
            Assert.GreaterOrEqual(types.Count, 2);
            types = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(0);
            Assert.GreaterOrEqual(types.Count, 1);

        }
        [Test]
        public void Find()
        {

            CommodityType fruit = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.Find(1);
            Assert.AreEqual("Food", fruit.TypeName);

        }
    }
}
