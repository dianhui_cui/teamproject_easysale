﻿using EasyModel.business;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEasySaleDbMySql
{
    [TestFixture]
    public class TestOrderProvider
    {
        [Test]
        public void AddOrder()
        {
            Order order = EasySaleDataProvider.EasySaleDataFactory.Single.Database.OrderOperator.Find(5);
            if (null == order)
            {
                order = new Order()
                {
                    CustomerFullName = "Ferry Smith",
                    CustomerID = 1,
                    OrderTime = DateTime.Now,
                    Total = 32.7
                };
                List<OrderItem> items = new List<OrderItem>();
                OrderItem item1 = new OrderItem()
                {
                    CommID = 3,
                    CommName = "Apple Gala",
                    GST = 0,
                    ItemCount = 10,
                    PST = 0,
                    UnitPrice = 1.69,
                    SubTotal = 16.9
                };
                OrderItem item2 = new OrderItem()
                {
                    CommID = 4,
                    CommName = "Lattuce",
                    GST = 0,
                    ItemCount = 20,
                    PST = 0,
                    UnitPrice = 0.79,
                    SubTotal = 15.8
                };
                items.Add(item1);
                items.Add(item2);
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.OrderOperator.Add(order, items);
                Assert.AreEqual(5, order.ID);

                OrderItem item3 = new OrderItem()
                {
                    OrderID = order.ID,
                    CommID = 2,
                    CommName = "Potato",
                    GST = 0,
                    ItemCount = 100,
                    PST = 0,
                    UnitPrice = 0.69,
                    SubTotal = 69
                };
                order.Total += 69;
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.OrderItemOperator.Add(item3);
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.OrderOperator.Update(order);

                order = EasySaleDataProvider.EasySaleDataFactory.Single.Database.OrderOperator.Find(5);
                
            }
            Assert.AreEqual(101.7, order.Total);
        }
        [Test]
        public void FindByCustomer()
        {
            List<Order> orders = EasySaleDataProvider.EasySaleDataFactory.Single.Database.OrderOperator.FindByCustomerID(1);
            Assert.GreaterOrEqual(orders.Count, 1);
        }
        [Test]
        public void FindByID()
        {
            Order order = EasySaleDataProvider.EasySaleDataFactory.Single.Database.OrderOperator.Find(5);
            Assert.AreEqual(101.7, order.Total);
        }
        [Test]
        public void FindByTime()
        {
            List<Order> orders = EasySaleDataProvider.EasySaleDataFactory.Single.Database.OrderOperator.FindByTime(DateTime.Parse("2020-04-06"),DateTime.Parse("2020-04-08"));
            Assert.GreaterOrEqual(orders.Count, 4);
        }

    }
}
