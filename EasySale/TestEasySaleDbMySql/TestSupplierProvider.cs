﻿using EasyModel.business;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEasySaleDbMySql
{
    [TestFixture]
    public class TestSupplierProvider
    {
        [Test]
        public void TestAddSupplier()
        {
            /*
            Supplier supplier = new Supplier() {
                Address = "16555 Rue Sait Charles",
                City="Beconsfield",
                Company="TVS",
                ContactName="Terry Smith",
                Country="Canada",
                Email="terry.smith@hotmail.com",
                Memo="",
                PostalCode="T8U5R7",
                ProductTypes="Dailys",
                Province="Quebec",
                Telephone="514-875-8888"
            };
            
            EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.Add(supplier);
            */
            Supplier destSupplier = EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.Find(2);
            Assert.AreEqual(destSupplier.ContactName, "Terry Smith");



        }
        [Test]
        public void FindSupplierReturnsNull()
        {
            Supplier destSupplier = EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.Find(1000);
            Assert.AreEqual(destSupplier,null);
        }
        [Test]
        public void FindAllSuppliers()
        {

            List<Supplier> suppliers = EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.FindAll();
            Assert.Greater(suppliers.Count, 1);
        }
        [Test]
        public void TestUpdate()
        {
            Supplier supplier = EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.Find(2);
            supplier.Company = "Face Book Canada";
            EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.Update(supplier);
            supplier = EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.Find(2);
            Assert.AreEqual("Face Book Canada", supplier.Company);
        }

    }
}
