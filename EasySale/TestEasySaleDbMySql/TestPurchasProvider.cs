﻿using EasyModel.business;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEasySaleDbMySql
{
    [TestFixture]
    public class TestPurchasProvider
    {
        [Test]
        public void AddPurchase()
        {
            Purchase purchase = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Find(1);
            if (null == purchase)
            {
                purchase = new Purchase()
                {
                    CommID = 3,
                    CommName = "Apple Gala Canada",
                    CommNumber = 200,
                    IsPurchased = false,
                    OrderDate = DateTime.Today,
                    Price = 1.09,
                    SupplierID = 1,
                    SupplierName = "Google",
                    Total = 308
                };
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Add(purchase);

                Assert.AreEqual(1, purchase.ID);

                purchase = new Purchase()
                {
                    CommID = 3,
                    CommName = "Apple Gala Canada",
                    CommNumber = 400,
                    IsPurchased = false,
                    OrderDate = DateTime.Today,
                    Price = 0.99,
                    SupplierID = 1,
                    SupplierName = "Google",
                    Total = 396
                };
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Add(purchase);
                Assert.AreEqual(2, purchase.ID);
                purchase = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Find(1);

            }

            Assert.AreEqual(200, purchase.CommNumber);
            Assert.AreEqual(1.09, purchase.Price);


        }
        [Test]
        public void UpdatePurchase()
        {
            DateTime now = DateTime.Now;
            Purchase purchase = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Find(2);
            if (!purchase.IsPurchased)
            {
                purchase.IsPurchased = true;
                purchase.PurchaseDate = now;
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Update(purchase);
            }
            else
            {
                now = purchase.PurchaseDate.Value;
            }
            purchase = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Find(2);
            Assert.AreEqual(now, purchase.PurchaseDate);
            Assert.AreEqual(true, purchase.IsPurchased);
        }
        [Test]
        public void FindByID()
        {
            Purchase purchase = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Find(1);
            Assert.AreEqual(200, purchase.CommNumber);
            Assert.AreEqual(1.09, purchase.Price);
        }

        [Test]
        public void FindBySupplier()
        {
            List<Purchase> purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.findBySupplier(1);
            Assert.GreaterOrEqual(purchases.Count, 2);
            purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.findBySupplier(1, true);
            Assert.GreaterOrEqual(purchases.Count, 1);
        }
        [Test]
        public void FindAll()
        {
            List<Purchase> purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.FindAll();
            Assert.GreaterOrEqual(purchases.Count, 2);
            purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.FindAll(true);
            Assert.GreaterOrEqual(purchases.Count, 1);
            purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.FindAll(false);
            Assert.GreaterOrEqual(purchases.Count, 1);
        }
        [Test]
        public void FindReturnNull()
        {
            List<Purchase> purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.findBySupplier(10000);
            Assert.AreEqual(0, purchases.Count);
            Purchase purchase = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Find(10000);
            Assert.AreEqual(null, purchase);
        }
    }
}
