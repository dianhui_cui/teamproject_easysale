﻿using EasyModel.business;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEasySaleDbMySql
{
    [TestFixture]
    public class TestCommodityProvider
    {
        [Test]
        public void AddCommodity()
        {
            Commodity apple = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.Find(3);
            if (apple == null)
            {
                apple = new Commodity()
                {
                    Barcode = "",
                    CommCode = 1101,
                    CommType = 11,
                    MemberPrice = 1.69,
                    Name = "Apple Gala Canada",
                    //Picture = new byte[0],
                    Price = 1.99,
                    Quantity = 0,
                    Unit = CommodityUnit.lb
                };
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.Add(apple);
                apple = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.Find(3);

            }
            Assert.AreEqual("Apple Gala Canada", apple.Name);
            Assert.AreEqual("Fruit", apple.CommTypeName);
         
            
        }
        [Test]
        public void TestFindAll()
        {
            List<Commodity> commodities = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindAll();
            Assert.GreaterOrEqual(commodities.Count, 3);
        }
        [Test]
        public void FindReturnNull()
        {
            Commodity commodity = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.Find(10000);
            Assert.AreEqual(null, commodity);
            commodity = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindByBarcode("999999999999");
            Assert.AreEqual(null, commodity);
            commodity = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindByCommCode(9999);
            Assert.AreEqual(null, commodity);

        }
        [Test]
        public void FindByCommCode()
        {
            Commodity commodity = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindByCommCode(4173);
            Assert.AreEqual("Apple Gala Canada", commodity.Name);
            Assert.AreEqual("Fruit", commodity.CommTypeName);
        }
        [Test]
        public void FindByBarcode()
        {
            Commodity commodity = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindByBarcode("1001");
            Assert.AreEqual("Potato", commodity.Name);
            Assert.AreEqual("Vegetable", commodity.CommTypeName);
        }
        [Test]
        public void FindByName()
        {
            List<Commodity> commodities = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindByName("la");
            Assert.GreaterOrEqual(commodities.Count, 2);
        }

    }
}
