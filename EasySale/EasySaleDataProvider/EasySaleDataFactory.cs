﻿using EasyModel.database;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EasySaleDataProvider
{
    public class EasySaleDataFactory
    {
        private static EasySaleDataFactory _factory;
        public static EasySaleDataFactory Single
        {
            get
            {
                if (_factory == null)
                {
                    _factory = new EasySaleDataFactory();
                }
                return _factory;
            }
        }
        private  IEasyDatabase _database;
        private ILog _log = LogManager.GetLogger(typeof(EasySaleDataFactory));
        public IEasyDatabase Database
        {
            get
            {
                if (_database == null)
                {
                    try
                    {
                        string dbClass = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location).AppSettings.Settings["Database"].Value;
                        _database = Utility.ReflectClass(dbClass) as IEasyDatabase;
                    }
                    catch (Exception ex)
                    {
                        _log.Error("Create database instance error:" + ex.StackTrace);
                    }
                }
                return _database;
            }
        }
        public void CloseDatabase()
        {
            if (null != _database)
            {
                _database.Close();
            }
        }

    }
}
