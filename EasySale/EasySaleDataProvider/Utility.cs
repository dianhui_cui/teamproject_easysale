﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EasySaleDataProvider
{
    public class Utility
    {
        private static ILog _log = LogManager.GetLogger(typeof(Utility));
        public static object ReflectClass(String className)
        {
            try
            {
                _log.Warn(string.Format("begin ReflectClass[{0}]", className));
                Type type = null;
                try
                {
                    type = Type.GetType(className);
                    if (null == type)
                    {
                        throw new ArgumentException(className);
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(ex.Message, ex);
                }

                Type[] paramTypes = System.Type.EmptyTypes;
                ConstructorInfo consInfo = type.GetConstructor(paramTypes);
                object[] paramArray = new object[0];
                return consInfo.Invoke(paramArray);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                _log.Error(ex.StackTrace, ex);
            }
            return null;

        }
     
    }
}
