﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyDatabase.objects
{
    public class Supplier
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string Company { get; set; }
        [Required]
        public string ContactName {get;set;}
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        [Required]
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string ProductTypes { get; set; }
        public string Memo { get; set; }
    }
}
