﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyDatabase.objects
{
    public class Purchase
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [ForeignKey("SupplierID")]
        public virtual Supplier Supplier { get; set; }
        [Required]
        [ForeignKey("CommID")]
        public virtual Commodity Commodity { get; set; }
        [Required]
        public double CommNumber { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public double Total { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
      
        public DateTime? PurchaseDate { get; set; }
    }
}
