﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace EasyDatabase.objects
{
    public class Customer
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set;}
        public string Province { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        [Required]
        public string Telephone { get; set; }
        public string Email { get; set; }
        [Required]
        public string MemberCardNo { get; set; }
        [Required]
        public double Balance { get; set; }
    }
}
