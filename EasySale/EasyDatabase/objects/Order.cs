﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace EasyDatabase.objects
{
    public class Order
    {
        [Key]
         public int ID { get; set; }
       
        [ForeignKey("CustomerID")]
        public virtual Customer Customer { get; set; }
        [Required]
        public DateTime OrderTime { get; set; }
        [Required]
        public double Total { get; set; }
    }
}
