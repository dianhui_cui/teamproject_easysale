﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace EasyDatabase.objects
{
    public class CommodityType
    {
        [Key]
        public int TypeCode { get; set; }
        [ForeignKey("ParentCode")]
        public virtual CommodityType Parent { get; set; }
        [Required]
        public string TypeName { get; set; }
        [Required]
        public double GST { get; set; }
        [Required]
        public double PST { get; set; }
    }
}
