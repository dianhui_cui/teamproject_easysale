﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace EasyDatabase.objects
{
    public class Commodity
    {
        [Key]
        public int ID { get; set; }
        public string Barcode { get; set; }
        public int CommCode { get; set; }
        [Required]
        [ForeignKey("CommType")]
        public virtual CommodityType CommodityType { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public double MemberPrice { get; set; }
        [Required]
        public double Quantity { get; set; }
        public byte[] Picture { get; set; }
        [Required]
        public string Unit { get; set; }
    }
}
