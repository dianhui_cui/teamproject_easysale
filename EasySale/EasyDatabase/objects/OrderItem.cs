﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace EasyDatabase.objects
{
    public class OrderItem
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [ForeignKey("OrderID")]
        public virtual Order Order { get; set; }
        [Required]
        [ForeignKey("CommID")]
        public virtual Commodity Commodity { get; set; }
        [Required]
        public double ItemCount { get; set; }
        [Required]
        public double UnitPrice { get; set; }
        [Required]
        public double SubTotal { get; set; }
    }
}
