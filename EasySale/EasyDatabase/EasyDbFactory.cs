﻿using EasyDatabase.mysql;
using EasyDatabase.sqlserver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyDatabase
{
    public class EasyDbFactory
    {
        private static EasyDbFactory _factory;
        public static EasyDbFactory Single
        {
            get
            {
                if (_factory == null)
                {
                    _factory = new EasyDbFactory();
                }
                return _factory;
            }
        }
        private Dictionary<string, EasyDbContexBase> contextDictionary = new Dictionary<string, EasyDbContexBase>();

        public EasyDbContexBase GetDbContext(DatabaseType dbType,string connectionString)
        {
            if (!contextDictionary.ContainsKey(connectionString))
            {
                switch (dbType)
                {
                    case DatabaseType.MySql:
                        contextDictionary.Add(connectionString, new EasyDbMySql(connectionString));
                        break;
                    case DatabaseType.SqlServer:
                        contextDictionary.Add(connectionString, new EasyDbSqlServer(connectionString));
                        break;
                }
               
            }
            return contextDictionary[connectionString];
        }
    }
}
