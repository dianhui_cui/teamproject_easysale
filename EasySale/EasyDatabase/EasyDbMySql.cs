﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyDatabase.mysql
{
    [DbConfigurationType(typeof(MySql.Data.EntityFramework.MySqlEFConfiguration))]
    public class EasyDbMySql : EasyDbContexBase
    {
        public EasyDbMySql(string connectionString) : base(connectionString)
        {
            
        }
        
    }
}
