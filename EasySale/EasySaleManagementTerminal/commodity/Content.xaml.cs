﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.commodity
{
    /// <summary>
    /// Interaction logic for Content.xaml
    /// </summary>
    public partial class Content : UserControl
    {
        private Dictionary<string, UserControl> subControls = new Dictionary<string, UserControl>();
        public Content()
        {
            InitializeComponent();
        }
        private void DockPanel_Loaded(object sender, RoutedEventArgs e)
        {
            InitContent();
        }
        private void InitContent()
        {
            if (!subControls.ContainsKey("listCommodity"))
            {
                CommodityListView lvCommodity = new CommodityListView();
                lvCommodity.Commodities = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindAll();
                lvCommodity.SelectedCommodityChanged += LvCommodity_SelectedCommodityChanged;
                docPanel.Children.Add(lvCommodity);
                subControls.Add("listCommodity", lvCommodity);
            }
        }
        private void LvCommodity_SelectedCommodityChanged(Commodity selectCommodity)
        {
            AddEditCommodity edit = docPanel.Children.OfType<AddEditCommodity>().FirstOrDefault();
            if (null == edit) return;
            edit.Commodity = selectCommodity;
        }

        public void AddCommodity()
        {

            docPanel.Children.Clear();
            if (!subControls.ContainsKey("editCommodity"))
            {
                AddEditCommodity addEdit = new AddEditCommodity();
                addEdit.ParentUc = this;
                subControls.Add("editCommodity", addEdit);
                DockPanel.SetDock(addEdit, Dock.Right);
            }
            (subControls["editCommodity"] as AddEditCommodity).ClearInputs();
            docPanel.Children.Add(subControls["editCommodity"]);
            docPanel.Children.Add(subControls["listCommodity"]);
        }
        public void AddCommodityType()
        {

            docPanel.Children.Clear();
            if (!subControls.ContainsKey("editCommodityType"))
            {
                AddEditCommodityType addEdit = new AddEditCommodityType();
                addEdit.ParentUc = this;
                subControls.Add("editCommodityType", addEdit);
                DockPanel.SetDock(addEdit, Dock.Right);
            }
            if (!subControls.ContainsKey("listCommodityType"))
            {
                CommodityTypeListView lvCommodityType = new CommodityTypeListView();
                lvCommodityType.CommodityTypes = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindAll();
                lvCommodityType.SelectedCommodityTypeChanged += LvCommodityType_SelectedCommodityTypeChanged;
                subControls.Add("listCommodityType", lvCommodityType);
            }
            (subControls["editCommodityType"] as AddEditCommodityType).ClearInputs();
            docPanel.Children.Add(subControls["editCommodityType"]);
            docPanel.Children.Add(subControls["listCommodityType"]);
        }
        private void LvCommodityType_SelectedCommodityTypeChanged(CommodityType selectCommodityType)
        {
            AddEditCommodityType edit = docPanel.Children.OfType<AddEditCommodityType>().FirstOrDefault();
            if (null == edit) return;
            edit.CommodityType = selectCommodityType;
        }
        public void UpdateCommodity()
        {
            Commodity selectCommodity = (subControls["listCommodity"] as CommodityListView).SelectedCommodity;
            if (selectCommodity == null) return;
            docPanel.Children.Clear();

            if (!subControls.ContainsKey("editCommodity"))
            {
                AddEditCommodity addEdit = new AddEditCommodity();
                addEdit.ParentUc = this;
                subControls.Add("editCommodity", addEdit);
                DockPanel.SetDock(addEdit, Dock.Right);
            }
            (subControls["editCommodity"] as AddEditCommodity).ClearInputs();
            (subControls["editCommodity"] as AddEditCommodity).Commodity = selectCommodity;
            docPanel.Children.Add(subControls["editCommodity"]);
            docPanel.Children.Add(subControls["listCommodity"]);
        }
        public void UpdateCommodityType()
        {
            CommodityType selectCommodityType = (subControls["listCommodityType"] as CommodityTypeListView).SelectedCommodityType;
            if (selectCommodityType == null) return;
            docPanel.Children.Clear();

            if (!subControls.ContainsKey("editCommodityType"))
            {
                AddEditCommodityType addEdit = new AddEditCommodityType();
                addEdit.ParentUc = this;
                subControls.Add("editCommodityType", addEdit);
                DockPanel.SetDock(addEdit, Dock.Right);
            }
            if (!subControls.ContainsKey("listCommodityType"))
            {
                CommodityTypeListView lvCommodityType = new CommodityTypeListView();
                lvCommodityType.CommodityTypes = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindAll();
                lvCommodityType.SelectedCommodityTypeChanged += LvCommodityType_SelectedCommodityTypeChanged;
                subControls.Add("listCommodityType", lvCommodityType);
            }
          (subControls["editCommodityType"] as AddEditCommodityType).ClearInputs();
            (subControls["editCommodityType"] as AddEditCommodityType).CommodityType = selectCommodityType;
            docPanel.Children.Add(subControls["editCommodityType"]);
            docPanel.Children.Add(subControls["listCommodityType"]);
        }
        public void SearchCommodity(string searchText)
        {
            CommodityListView listCommodity = docPanel.Children.OfType<CommodityListView>().FirstOrDefault();
            if (null == listCommodity)
            {
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["listCommodity"]);
            }
            else
            {

                if (docPanel.Children.Count > 1)
                {
                    docPanel.Children.Clear();
                    docPanel.Children.Add(subControls["listCommodity"]);
                }
            }
            (subControls["listCommodity"] as CommodityListView).Search(searchText);
        }
        public void FilterCommodityByType(int commodityType)
        {
            if (!subControls.ContainsKey("listCommodity")) return;
            CommodityListView listCommodity = docPanel.Children.OfType<CommodityListView>().FirstOrDefault();
            if (null == listCommodity)
            {
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["listCommodity"]);
            }
            else
            {

                if (docPanel.Children.Count > 1)
                {
                    docPanel.Children.Clear();
                    docPanel.Children.Add(subControls["listCommodity"]);
                }
            }
          (subControls["listCommodity"] as CommodityListView).FilterByCommodityType(commodityType);
        }
        public void SearchCommodityType(string searchText)
        {
            if (!subControls.ContainsKey("listCommodityType")) return;
            CommodityTypeListView listCommodityType = docPanel.Children.OfType<CommodityTypeListView>().FirstOrDefault();
            if (null == listCommodityType)
            {
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["listCommodityType"]);
            }
            else
            {

                if (docPanel.Children.Count > 1)
                {
                    docPanel.Children.Clear();
                    docPanel.Children.Add(subControls["listCommodityType"]);
                }
            }
           (subControls["listCommodityType"] as CommodityTypeListView).Search(searchText);
        }
        public void RefreshCommodity()
        {
            //lvCustomer.Customers
            (subControls["listCommodity"] as CommodityListView).Commodities = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindAll();
        }
        public void RefreshCommodityType()
        {
            //lvCustomer.Customers
            (subControls["listCommodityType"] as CommodityTypeListView).CommodityTypes = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindAll();
        }
        public void ClearCommoditySelection()
        {
            (subControls["listCommodity"] as CommodityListView).ClearSelctionCommodity();
        }
        public void ClearCommodityTypeSelection()
        {
            (subControls["listCommodityType"] as CommodityTypeListView).ClearSelctionCommodityType();
        }
        public void ListChanged(string listName)
        {
            if (listName == "Commodity")
            {
                if (!subControls.ContainsKey("listCommodity"))
                {
                    CommodityListView lvCommodity = new CommodityListView();
                    lvCommodity.Commodities = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindAll();
                    lvCommodity.SelectedCommodityChanged += LvCommodity_SelectedCommodityChanged;
                    docPanel.Children.Add(lvCommodity);
                    subControls.Add("listCommodity", lvCommodity);
                }
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["listCommodity"]);
            }
            else
            {
                docPanel.Children.Clear();
               
                if (!subControls.ContainsKey("listCommodityType"))
                {
                    CommodityTypeListView lvCommodityType = new CommodityTypeListView();
                    lvCommodityType.CommodityTypes = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindAll();
                    lvCommodityType.SelectedCommodityTypeChanged += LvCommodityType_SelectedCommodityTypeChanged;
                    subControls.Add("listCommodityType", lvCommodityType);
                }
                docPanel.Children.Add(subControls["listCommodityType"]);
            }
        }
    }
}
