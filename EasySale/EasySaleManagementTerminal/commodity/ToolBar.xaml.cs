﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.commodity
{
    /// <summary>
    /// Interaction logic for ToolBar.xaml
    /// </summary>
    public partial class ToolBar : UserControl
    {
        public event Action AddCommodityEvent;
        public event Action UpdateCommodityEvent;
        public event Action AddCommodityTypeEvent;
        public event Action UpdateCommodityTypeEvent;
        public event Action<string> SearchCommodityEvent;
        public event Action<string> SearchCommodityTypeEvent;
        public event Action<int> FilterCommodityEvent;
        public event Action<string> ListChangedEvent;
        public ToolBar()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            tbSearchText.Text = "";

            AddCommodityEvent?.Invoke();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            tbSearchText.Text = "";
            UpdateCommodityEvent?.Invoke();
        }

        private void tbSearchText_TextChanged(object sender, TextChangedEventArgs e)
        {
            SearchCommodityEvent?.Invoke(tbSearchText.Text);
        }
        private void btAddCommodityType_Click(object sender, RoutedEventArgs e)
        {
            tbSearchCommodityTypeText.Text = "";
            AddCommodityTypeEvent?.Invoke();
        }

        private void btUpdateCommodityType_Click(object sender, RoutedEventArgs e)
        {
            tbSearchCommodityTypeText.Text = "";
            UpdateCommodityTypeEvent?.Invoke();
        }

        private void tbSearchCommodityTypeText_TextChanged(object sender, TextChangedEventArgs e)
        {
            SearchCommodityTypeEvent?.Invoke(tbSearchCommodityTypeText.Text);
        }

        private void comboCommodityType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboCommodityType.SelectedItem == null) return;
            CommodityType commodityType = comboCommodityType.SelectedItem as CommodityType;
            FilterCommodityEvent?.Invoke(commodityType.TypeCode);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            List<CommodityType> types = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindAll();
            types.Insert(0, new CommodityType() { TypeCode = 0, TypeName = "--All Types--", ParentCode = 0 });
            comboCommodityType.ItemsSource = types;
            comboCommodityType.SelectedIndex = 0;
        }

        private void rbCommodity_Checked(object sender, RoutedEventArgs e)
        {
            rbCommodityType.IsChecked = false;
            ListChangedEvent?.Invoke("Commodity");
        }

        private void rbCommodityType_Checked(object sender, RoutedEventArgs e)
        {
            rbCommodity.IsChecked = false;
            ListChangedEvent?.Invoke("CommodityType");
        }
    }
}
