﻿using EasyModel.business;
using EasyModel.utility;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.commodity
{
    /// <summary>
    /// Interaction logic for AddEditCommodity.xaml
    /// </summary>
    public partial class AddEditCommodity : UserControl
    {
        private Commodity _commodity;
        private bool _pictureChanged=false;
        public AddEditCommodity()
        {
            InitializeComponent();
        }
        public Commodity Commodity
        {
            set
            {
                _commodity = value;
                SetInputs();
            }
        }
        public Content ParentUc { get; set; }
        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            ClearInputs();
            ParentUc.ClearCommoditySelection();
        }
        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            string barcode = tbBarcode.Text;
            CommodityType ct = (CommodityType)comboSubCategory.SelectedItem;
            if (ct.TypeCode == 0)
            {
                MessageBox.Show( "Please select a sub category", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            int commType = ct.TypeCode;
            string commTypeName = ct.TypeName;
            string name = tbName.Text;
            double price;
            if (!double.TryParse(tbPrice.Text, out price))
            {
                MessageBox.Show( "Wrong price", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (price <= 0)
            {
                MessageBox.Show( "Wrong price", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double memberPrice; 
      
            if (!double.TryParse(tbMemberPrice.Text, out memberPrice))
            {
                MessageBox.Show("Wrong member price", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (memberPrice <= 0)
            {
                MessageBox.Show("Wrong member price", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            int commCode;
            if (!int.TryParse(tbCommCode.Text, out commCode))
            {
                MessageBox.Show( "Wrong commCode", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            /*double quantity;
            if (!double.TryParse(tbQty.Text, out quantity))
            {
                MessageBox.Show(this, "Wrong quantity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (quantity <= 0)
            {
                MessageBox.Show(this, "Wrong quantity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }*/
            byte[] picture;
            if (imgPhoto.Source == null)
            {
                picture = new byte[0];
            }
            else
            {
                //picture = bitmapImageToByteArray((BitmapImage)imgPhoto.Source);
                picture = Utility.ImageSourceToBytes(imgPhoto.Source);
            }

            CommodityUnit unit = (CommodityUnit)comboUnit.SelectedItem;

            if (_commodity == null)
            {
                _commodity = new Commodity()
                {
                    CommCode = commCode,
                    Barcode = barcode,
                    CommType = commType,
                    CommTypeName = commTypeName,
                    MemberPrice = memberPrice,
                    Name = name,
                    //Picture = picture,
                    Price = price,
                    Unit = unit
                };
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.Add(_commodity);
                if (null == picture || picture.Length == 0) { }
                else
                {
                    EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.SetPicture(_commodity.ID, picture);
                }
                ParentUc.RefreshCommodity();
            }
            else
            {
                _commodity.CommCode = commCode;
                _commodity.Barcode = barcode;
                _commodity.CommType = commType;
                _commodity.CommTypeName = commTypeName;
                _commodity.MemberPrice = memberPrice;
                _commodity.Name = name;
                //_commodity.Picture = picture;
                _commodity.Price = price;
                _commodity.Unit = unit;
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.Update(_commodity);
                if (_pictureChanged)
                {
                    EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.SetPicture(_commodity.ID, picture);
                }
                ParentUc.RefreshCommodity();

            }
            ClearInputs();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (comboCategory.Items.Count == 0)
            {
                LoadCommodityTypes();
            }
         
        }
        private void LoadCommodityTypes()
        {
            comboUnit.ItemsSource = Enum.GetValues(typeof(EasyModel.business.CommodityUnit));
            List<CommodityType> topLevelTypes = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(0);
            topLevelTypes.Add(new CommodityType() { TypeCode = 0, TypeName = "--No top level--" });
            comboCategory.ItemsSource = topLevelTypes;
            comboCategory.SelectedIndex = 0;
        }
     

        public void SetInputs()
        {
            if (comboCategory.Items.Count == 0)
            {
                LoadCommodityTypes();
            }
            lblId.Content = _commodity.ID;
          
            int commTypeID = _commodity.CommType;
            CommodityType commType = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.Find(commTypeID);
            if (commType.ParentCode == 0)
            {
                SetSelectItem(comboCategory, 0);
                SetSelectItem(comboSubCategory, commTypeID);
            }
            else
            {
                SetSelectItem(comboCategory, commType.ParentCode);
                SetSelectItem(comboSubCategory, commTypeID);
            }
            tbName.Text = _commodity.Name;
            tbPrice.Text = _commodity.Price.ToString();
            tbMemberPrice.Text = _commodity.MemberPrice.ToString();
            comboUnit.SelectedItem = _commodity.Unit;
            tbBarcode.Text = _commodity.Barcode;
            tbCommCode.Text = _commodity.CommCode.ToString();
            byte[] picture = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.GetPicture(_commodity.ID);
            imgPhoto.Source = Utility.ByteArrayToImage(picture);
        }
        public void ClearInputs()
        {
            _commodity = null;
            lblId.Content ="-";
            SetSelectItem(comboCategory, 0);
            SetSelectItem(comboSubCategory, 0);
            tbName.Text = "";
            tbPrice.Text = "";
            tbMemberPrice.Text = "";
            comboUnit.SelectedIndex =0;
            tbBarcode.Text = "";
            tbCommCode.Text = "";
            imgPhoto.Source = null;
        }
        private void SetSelectItem(ComboBox comboBox, int commType)
        {
            foreach (var item in comboBox.Items)
            {
                if ((item as CommodityType).TypeCode == commType)
                {
                    comboBox.SelectedItem = item;
                    break;
                }
            }
        }
        private void comboCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CommodityType topType = comboCategory.SelectedItem as CommodityType;
            List<CommodityType> subLevelTypes = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(topType.TypeCode);
            subLevelTypes.Insert(0,new CommodityType() { TypeCode = 0, TypeName = "--Not set--" });
            comboSubCategory.ItemsSource = subLevelTypes;
            comboSubCategory.SelectedIndex = 0;
        }
        private void btAddPhoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "images |*.gif;*.png;*.jpg|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    // imageInBytes = File.ReadAllBytes(openFileDialog.FileName);
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(openFileDialog.FileName);
                    bitmap.EndInit();
                    imgPhoto.Source = bitmap;
                    _pictureChanged = true;
                }
                catch (IOException ex)
                {
                    MessageBox.Show( "Error Reading file", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (UriFormatException ex)
                {
                    MessageBox.Show( "Error Reading file", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

    }
}
