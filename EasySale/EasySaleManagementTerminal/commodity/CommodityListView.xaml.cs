﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.commodity
{
    /// <summary>
    /// Interaction logic for CommodityListView.xaml
    /// </summary>
    public partial class CommodityListView : UserControl
    {
        private List<Commodity> commodities;
        public event Action<Commodity> SelectedCommodityChanged;
        public CommodityListView()
        {
            InitializeComponent();
        }
        public List<Commodity> Commodities
        {
            set
            {
                commodities = value;
                lvCommodities.ItemsSource = commodities;
            }
        }
        public Commodity SelectedCommodity
        {
            get
            {
                Commodity commodity = lvCommodities.SelectedItem as Commodity;
                return commodity;
            }
        }
        public void Search(string searchText)
        {

            if (string.IsNullOrWhiteSpace(searchText))
            {
                lvCommodities.ItemsSource = commodities;
            }
            else
            {
                searchText = searchText.ToLower();

                lvCommodities.ItemsSource = (from commodity in commodities where commodity.Name.ToLower().Contains(searchText) || (commodity.Barcode!=null&&commodity.Barcode.ToLower().Contains(searchText)) || commodity.CommCode.ToString().ToLower().Contains(searchText) select commodity).ToList();
            }
        }
        public void FilterByCommodityType(int commodityType)
        {

            if (commodityType==0)
            {
                lvCommodities.ItemsSource = commodities;
            }
            else
            {
             
                lvCommodities.ItemsSource = (from commodity in commodities where commodity.CommType==commodityType select commodity).ToList();
            }
        }
        public void ClearSelctionCommodity()
        {
            lvCommodities.UnselectAll();
        }

        private void lvCommodities_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Commodity commodity = lvCommodities.SelectedItem as Commodity;
            if (null == commodity) return;
            SelectedCommodityChanged?.Invoke(commodity);
        }
    }
}
