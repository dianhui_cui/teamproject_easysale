﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.commodity
{
    /// <summary>
    /// Interaction logic for CommodityTypeListView.xaml
    /// </summary>
    public partial class CommodityTypeListView : UserControl
    {
        private List<CommodityType> commodityTypes;
        public event Action<CommodityType> SelectedCommodityTypeChanged;
        public CommodityTypeListView()
        {
            InitializeComponent();
        }
        public List<CommodityType> CommodityTypes
        {
            set
            {
                commodityTypes = value;
                lvCommodityTypes.ItemsSource = commodityTypes;
            }
        }
        public CommodityType SelectedCommodityType
        {
            get
            {
                CommodityType commodityType = lvCommodityTypes.SelectedItem as CommodityType;
                return commodityType;
            }
        }
        public void Search(string searchText)
        {

            if (string.IsNullOrWhiteSpace(searchText))
            {
                lvCommodityTypes.ItemsSource = commodityTypes;
            }
            else
            {
                searchText = searchText.ToLower();
                lvCommodityTypes.ItemsSource = (from type in commodityTypes where type.TypeName.ToLower().Contains(searchText) || type.TypeCode.ToString().ToLower().Contains(searchText) select type).ToList();
            }
        }
     
        public void ClearSelctionCommodityType()
        {
            lvCommodityTypes.UnselectAll();
        }

        private void lvCommodityTypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CommodityType commodityType = lvCommodityTypes.SelectedItem as CommodityType;
            if (null == commodityType) return;
            SelectedCommodityTypeChanged?.Invoke(commodityType);
        }
    }
}
