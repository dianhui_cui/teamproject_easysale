﻿using EasyModel.ui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace EasySaleManagementTerminal.commodity
{
    public class CommodityModule : ModuleBase
    {
        private ToolBar _toolBar;
        private Content _content;
        public CommodityModule()
        {
            /*
            _toolBar = new ToolBar();
            _content = new Content();
            _toolBar.AddCommodityEvent += _content.AddCommodity;
            _toolBar.AddCommodityTypeEvent += _content.AddCommodityType;
            _toolBar.UpdateCommodityEvent += _content.UpdateCommodity;
            _toolBar.UpdateCommodityTypeEvent += _content.UpdateCommodityType;
            _toolBar.SearchCommodityEvent += _content.SearchCommodity;
            _toolBar.SearchCommodityTypeEvent += _content.SearchCommodityType;
            _toolBar.FilterCommodityEvent += _content.FilterCommodityByType;
            */
        }
        public override string Name
        {
            get
            {
                return "Commodity";
            }
        }
        public override UserControl ToolBar
        {
            get
            {
                if (_toolBar == null)
                {
                    _toolBar = new ToolBar();
                }
                return _toolBar;

            }
        }
        public override UserControl ModuleUI
        {
            get
            {
                if (_content == null)
                {
                    _content = new Content();
                    _toolBar.AddCommodityEvent += _content.AddCommodity;
                    _toolBar.AddCommodityTypeEvent += _content.AddCommodityType;
                    _toolBar.UpdateCommodityEvent += _content.UpdateCommodity;
                    _toolBar.UpdateCommodityTypeEvent += _content.UpdateCommodityType;
                    _toolBar.SearchCommodityEvent += _content.SearchCommodity;
                    _toolBar.SearchCommodityTypeEvent += _content.SearchCommodityType;
                    _toolBar.FilterCommodityEvent += _content.FilterCommodityByType;
                    _toolBar.ListChangedEvent += _content.ListChanged;

                }
                return _content;
            }

        }
    }
}

