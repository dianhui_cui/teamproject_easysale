﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.commodity
{
    /// <summary>
    /// Interaction logic for AddEditCommodityType.xaml
    /// </summary>
    public partial class AddEditCommodityType : UserControl
    {
        private CommodityType _commodityType;
        public AddEditCommodityType()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (comboParentType.Items.Count == 0)
            {
                LoadParentCommodityTypes();
            }
        }
        public CommodityType CommodityType
        {
            set
            {
                _commodityType = value;
                SetInputs();
            }
        }
        public Content ParentUc { get; set; }
        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            ClearInputs();
            ParentUc.ClearCommodityTypeSelection();
        }
        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            int typeCode;
            if (!int.TryParse(tbTypeCode.Text, out typeCode))
            {
                MessageBox.Show("Wrong type code", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (typeCode <= 0)
            {
                MessageBox.Show("Wrong type code", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string typeName = tbTypeName.Text;
            if (string.IsNullOrWhiteSpace(typeName))
            {
                MessageBox.Show("Wrong type name", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            CommodityType parentType = comboParentType.SelectedItem as CommodityType;
            double gst;
            if (!double.TryParse(tbGST.Text, out gst))
            {
                MessageBox.Show("Wrong GST", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (gst < 0)
            {
                MessageBox.Show("Wrong GST", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double pst;
            if (!double.TryParse(tbPST.Text, out pst))
            {
                MessageBox.Show("Wrong PST", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (pst < 0)
            {
                MessageBox.Show("Wrong PST", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
         

            if (_commodityType == null)
            {
                _commodityType = new CommodityType()
                {
                    TypeCode = typeCode,
                    TypeName = typeName,
                    ParentCode=parentType.TypeCode,
                    GST=gst,
                    PST=pst
                };
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.Add(_commodityType);
                ParentUc.RefreshCommodityType();
            }
            else
            {

                _commodityType.TypeName = typeName;
                _commodityType.ParentCode = parentType.TypeCode;
                _commodityType.GST = gst;
                _commodityType.PST = pst;
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.Update(_commodityType);
                ParentUc.RefreshCommodityType();

            }
            ClearInputs();
        }
        private void LoadParentCommodityTypes()
        {
            List<CommodityType> topLevelTypes = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(0);
            topLevelTypes.Add(new CommodityType() { TypeCode = 0, TypeName = "--No parent level--" });
            comboParentType.ItemsSource = topLevelTypes;
            comboParentType.SelectedIndex = 0;
        }


        public void SetInputs()
        {
            tbTypeCode.Text = _commodityType.TypeCode.ToString();
            tbTypeCode.IsEnabled = false;
            tbTypeName.Text = _commodityType.TypeName;
            SetSelectItem(comboParentType, _commodityType.ParentCode);
            tbGST.Text = _commodityType.GST.ToString();
            tbPST.Text = _commodityType.PST.ToString();
        }
        public void ClearInputs()
        {
            _commodityType = null;
            tbTypeCode.Text = "";
            tbTypeCode.IsEnabled = true;
            tbTypeName.Text = "";
            SetSelectItem(comboParentType, 0);
            tbGST.Text = "0";
            tbPST.Text = "0";
        }
        private void SetSelectItem(ComboBox comboBox, int commType)
        {
            foreach (var item in comboBox.Items)
            {
                if ((item as CommodityType).TypeCode == commType)
                {
                    comboBox.SelectedItem = item;
                    break;
                }
            }
        }
     
     

    }
}
