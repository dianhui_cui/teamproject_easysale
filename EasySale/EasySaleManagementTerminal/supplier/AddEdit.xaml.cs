﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.supplier
{
    /// <summary>
    /// Interaction logic for AddEdit.xaml
    /// </summary>
    public partial class AddEdit : UserControl
    {
        private Supplier _supplier;
        public AddEdit()
        {
            InitializeComponent();
        }
        public Supplier Supplier
        {
            set { _supplier = value;
                SetInputs();
            }
        }
        public Content ParentUc { get; set; }
        

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            if (tbCompany.Text == "")
            {
                MessageBox.Show("Company couldn't be empty", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            if (_supplier == null)
            {
                _supplier = new Supplier() { 
                    Address=tbAddress.Text,
                    City=tbCity.Text,
                    Company=tbCompany.Text,
                    ContactName=tbContact.Text,
                    Country=tbCountry.Text,
                    Email=tbEmail.Text,
                    Memo=tbMemo.Text,
                    PostalCode=tbPostalCode.Text,
                    ProductTypes=tbProducts.Text,
                    Province=tbProvince.Text,
                    Telephone=tbTelephone.Text
                };
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.Add(_supplier);
                ParentUc.RefreshSuppliers();
            }
            else {

                _supplier.Address = tbAddress.Text;
                     _supplier.City = tbCity.Text;
                _supplier.Company = tbCompany.Text;
                _supplier.ContactName = tbContact.Text;
                _supplier.Country = tbCountry.Text;
                _supplier.Email = tbEmail.Text;
                _supplier.Memo = tbMemo.Text;
                _supplier.PostalCode = tbPostalCode.Text;
                _supplier.ProductTypes = tbProducts.Text;
                _supplier.Province = tbProvince.Text;
                _supplier.Telephone = tbTelephone.Text;
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.Update(_supplier);
                ParentUc.RefreshSuppliers();

            }
            ClearInputs();
        }
        public void SetInputs()
        {
            lblID.Content = _supplier.ID;
            tbAddress.Text = _supplier.Address;
            tbCity.Text = _supplier.City;
            tbCompany.Text = _supplier.Company;
            tbContact.Text = _supplier.ContactName;
            tbCountry.Text = _supplier.Country;
            tbEmail.Text = _supplier.Email;
            tbMemo.Text = _supplier.Memo;
            tbPostalCode.Text = _supplier.PostalCode;
            tbProducts.Text = _supplier.ProductTypes;
            tbProvince.Text = _supplier.Province;
            tbTelephone.Text = _supplier.Telephone;
        }
        public void ClearInputs()
        {
            _supplier = null;
            lblID.Content ="-";
            tbAddress.Text ="";
            tbCity.Text = "";
            tbCompany.Text = "";
            tbContact.Text = "";
            tbCountry.Text = "";
            tbEmail.Text = "";
            tbMemo.Text = "";
            tbPostalCode.Text = "";
            tbProducts.Text = "";
            tbProvince.Text = "";
            tbTelephone.Text = "";
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            ClearInputs();
            ParentUc.ClearSelection();
        }
    }
}
