﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.supplier
{
    /// <summary>
    /// Interaction logic for Content.xaml
    /// </summary>
    public partial class Content : UserControl
    {
     
        private Dictionary<string, UserControl> subControls = new Dictionary<string, UserControl>();
        public Content()
        {
            InitializeComponent();
        }

        private void DockPanel_Loaded(object sender, RoutedEventArgs e)
        {
            InitContent();
        }
        private void InitContent()
        {
            if (!subControls.ContainsKey("list"))
            {
                SupplierListView lvSupplier = new SupplierListView();
                lvSupplier.Suppliers = EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.FindAll();
                lvSupplier.SelectedSupplierChanged += LvSupplier_SelectedSupplierChanged;
                docPanel.Children.Add(lvSupplier);
                subControls.Add("list", lvSupplier);
            }
        }

        private void LvSupplier_SelectedSupplierChanged(Supplier selectSupplier)
        {
            AddEdit edit= docPanel.Children.OfType<AddEdit>().FirstOrDefault();
            if (null==edit) return;
            edit.Supplier = selectSupplier;
        }

        public void AddSupplier()
        {

            docPanel.Children.Clear();
            if (!subControls.ContainsKey("edit"))
            {
                AddEdit addEdit = new AddEdit();
                addEdit.ParentUc = this;
                subControls.Add("edit", addEdit);
                DockPanel.SetDock(addEdit, Dock.Right);
            }
            (subControls["edit"] as AddEdit).ClearInputs();
            docPanel.Children.Add(subControls["edit"]);
            docPanel.Children.Add(subControls["list"]);
        }

        public void UpdateSupplier()
        {
            Supplier selectSupplier = (subControls["list"] as SupplierListView).SelectedSupplier;
            if (selectSupplier == null) return;
            docPanel.Children.Clear();

            if (!subControls.ContainsKey("edit"))
            {
                AddEdit addEdit = new AddEdit();
                addEdit.ParentUc = this;
                subControls.Add("edit", addEdit);
                DockPanel.SetDock(addEdit, Dock.Right);
            }
            (subControls["edit"] as AddEdit).ClearInputs();
            (subControls["edit"] as AddEdit).Supplier = selectSupplier;
            docPanel.Children.Add(subControls["edit"]);
            docPanel.Children.Add(subControls["list"]);
        }
        public void SearchSupplier(string searchText)
        {
            if (docPanel.Children.Count > 1)
            {
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["list"]);
            }
            (subControls["list"] as SupplierListView).Search(searchText);
        }
        public void RefreshSuppliers()
        {
            //lvSupplier.Suppliers
            (subControls["list"] as SupplierListView).Suppliers = EasySaleDataProvider.EasySaleDataFactory.Single.Database.SupplierOperator.FindAll();
        }
        public void ClearSelection()
        {
            (subControls["list"] as SupplierListView).ClearSelctionSupplier();
        }
    }
}
