﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.supplier
{
    /// <summary>
    /// Interaction logic for SupplierListView.xaml
    /// </summary>
    public partial class SupplierListView : UserControl
    {
        private List<Supplier> suppliers;
        public event Action<Supplier> SelectedSupplierChanged;
        public SupplierListView()
        {
            InitializeComponent();
        }
        public List<Supplier> Suppliers
        {
            set
            {
                suppliers = value;
                lvSuppliers.ItemsSource = suppliers;
            }
        }
        public Supplier SelectedSupplier {
            get {
                Supplier supplier = lvSuppliers.SelectedItem as Supplier;
                return supplier;
            }
        }
        public void Search(string searchText)
        {
            
            if (string.IsNullOrWhiteSpace(searchText))
            {
                lvSuppliers.ItemsSource = suppliers;
            }
            else
            {
                searchText = searchText.ToLower();
                lvSuppliers.ItemsSource = (from supplier in suppliers where supplier.Company.ToLower().Contains(searchText) || supplier.ContactName.ToLower().Contains(searchText) select supplier).ToList();
            }
        }
        public void ClearSelctionSupplier()
        {
            lvSuppliers.UnselectAll();
        }

     
        private void ListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double width = this.Width;
            colCompany.Width = width * 0.1;
            colContact.Width = width * 0.1;
            colTelephone.Width = width * 0.1;
            colProducts.Width = width * 0.1;
            colEmail.Width = width * 0.1;
            colAddress.Width = width * 0.1;
            colCity.Width = width * 0.1;
            colProvince.Width = width * 0.1;
            colCountry.Width = width * 0.1;
            colPostalCode.Width = width * 0.1;
        }

        private void lvSuppliers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Supplier supplier = lvSuppliers.SelectedItem as Supplier;
            if (null == supplier) return;
            SelectedSupplierChanged?.Invoke(supplier);
        }
    }
}
