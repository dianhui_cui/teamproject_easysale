﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.supplier
{
    /// <summary>
    /// Interaction logic for ToolBar.xaml
    /// </summary>
    public partial class ToolBar : UserControl
    {
        public event Action AddSupplierEvent;
        public event Action UpdateSupplierEvent;
        public event Action<string> SearchEvent;
        public ToolBar()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            tbSearchText.Text = "";
           
            AddSupplierEvent?.Invoke();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            tbSearchText.Text = "";
            UpdateSupplierEvent?.Invoke();
        }

        private void tbSearchText_TextChanged(object sender, TextChangedEventArgs e)
        {
            SearchEvent?.Invoke(tbSearchText.Text);
        }
    }
}
