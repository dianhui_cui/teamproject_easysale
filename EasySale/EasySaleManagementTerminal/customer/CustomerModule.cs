﻿using EasyModel.ui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace EasySaleManagementTerminal.customer
{
    public class CustomerModule : ModuleBase
    {
        private ToolBar _toolBar;
        private Content _content;
        public CustomerModule()
        {
            /*
            _toolBar = new ToolBar();
            _content = new Content();
            _toolBar.AddCustomerEvent += _content.AddCustomer;
            _toolBar.UpdateCustomerEvent += _content.UpdateCustomer;
            _toolBar.SearchEvent += _content.SearchCustomer;
            */
        }
        public override string Name
        {
            get
            {
                return "Customer";
            }
        }
        public override UserControl ToolBar
        {
            get
            {
                if (_toolBar == null)
                {
                    _toolBar = new ToolBar();
                }
                return _toolBar;

            }
        }
        public override UserControl ModuleUI
        {
            get
            {
                if (_content == null)
                {
                    _content = new Content();
                    _toolBar.AddCustomerEvent += _content.AddCustomer;
                    _toolBar.UpdateCustomerEvent += _content.UpdateCustomer;
                    _toolBar.SearchEvent += _content.SearchCustomer;
                }
                return _content;
            }

        }
    }
}
