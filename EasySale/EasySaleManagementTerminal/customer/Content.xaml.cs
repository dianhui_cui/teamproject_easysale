﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.customer
{
    /// <summary>
    /// Interaction logic for Content.xaml
    /// </summary>
    public partial class Content : UserControl
    {
        private Dictionary<string, UserControl> subControls = new Dictionary<string, UserControl>();
        public Content()
        {
            InitializeComponent();
        }

        private void DockPanel_Loaded(object sender, RoutedEventArgs e)
        {
            InitContent();
        }
        private void InitContent()
        {
            if (!subControls.ContainsKey("list"))
            {
                CustomerListView lvCustomer = new CustomerListView();
                lvCustomer.Customers = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.FindAll();
                lvCustomer.SelectedCustomerChanged += LvCustomer_SelectedCustomerChanged;
                docPanel.Children.Add(lvCustomer);
                subControls.Add("list", lvCustomer);
            }
        }

        private void LvCustomer_SelectedCustomerChanged(Customer selectCustomer)
        {
            AddEdit edit = docPanel.Children.OfType<AddEdit>().FirstOrDefault();
            if (null == edit) return;
            edit.Customer = selectCustomer;
        }

        public void AddCustomer()
        {

            docPanel.Children.Clear();
            if (!subControls.ContainsKey("edit"))
            {
                AddEdit addEdit = new AddEdit();
                addEdit.ParentUc = this;
                subControls.Add("edit", addEdit);
                DockPanel.SetDock(addEdit, Dock.Right);
            }
            (subControls["edit"] as AddEdit).ClearInputs();
            docPanel.Children.Add(subControls["edit"]);
            docPanel.Children.Add(subControls["list"]);
        }

        public void UpdateCustomer()
        {
            Customer selectCustomer = (subControls["list"] as CustomerListView).SelectedCustomer;
            if (selectCustomer == null) return;
            docPanel.Children.Clear();

            if (!subControls.ContainsKey("edit"))
            {
                AddEdit addEdit = new AddEdit();
                addEdit.ParentUc = this;
                subControls.Add("edit", addEdit);
                DockPanel.SetDock(addEdit, Dock.Right);
            }
            (subControls["edit"] as AddEdit).ClearInputs();
            (subControls["edit"] as AddEdit).Customer = selectCustomer;
            docPanel.Children.Add(subControls["edit"]);
            docPanel.Children.Add(subControls["list"]);
        }
        public void SearchCustomer(string searchText)
        {
            if (docPanel.Children.Count > 1)
            {
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["list"]);
            }
            (subControls["list"] as CustomerListView).Search(searchText);
        }
        public void RefreshCustomers()
        {
            //lvCustomer.Customers
            (subControls["list"] as CustomerListView).Customers = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.FindAll();
        }
        public void ClearSelection()
        {
            (subControls["list"] as CustomerListView).ClearSelctionCustomer();
        }
    }
}
