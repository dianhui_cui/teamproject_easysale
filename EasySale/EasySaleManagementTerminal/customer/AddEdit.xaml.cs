﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.customer
{
    /// <summary>
    /// Interaction logic for AddEdit.xaml
    /// </summary>
    public partial class AddEdit : UserControl
    {
        private Customer _customer;
        public AddEdit()
        {
            InitializeComponent();
        }
        public Customer Customer
        {
            set
            {
                _customer = value;
                SetInputs();
            }
        }
        public Content ParentUc { get; set; }

        public void SetInputs()
        {
            lblID.Content = _customer.ID;
            tbFirstName.Text = _customer.FirstName;
            tbLastName.Text = _customer.LastName;
            tbAddress.Text = _customer.Address;
            tbCity.Text = _customer.City;

            tbProvince.Text = _customer.Province;
            tbPostalCode.Text = _customer.PostalCode;
            tbCountry.Text = _customer.Country;
            tbTelephone.Text = _customer.Telephone;
            tbMemberCardNo.Text = _customer.MemberCardNo;
            tbEmail.Text = _customer.Email;
        }
        public void ClearInputs()
        {
            _customer = null;
            lblID.Content = "-";
            tbFirstName.Text = "";
            tbLastName.Text = "";
            tbAddress.Text = "";
            tbCity.Text = "";
            tbProvince.Text = "";
            tbPostalCode.Text = "";
            tbCountry.Text = "";
            tbTelephone.Text = "";
            tbMemberCardNo.Text = "";
            tbEmail.Text = "";
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            string fn = tbFirstName.Text;
            string ln = tbLastName.Text;
            string addr = tbAddress.Text;
            string city = tbCity.Text;
            string province = tbProvince.Text;
            string postalCode = tbPostalCode.Text;
            string country = tbCountry.Text;
            string telephone = tbTelephone.Text;
            string memNo = tbMemberCardNo.Text;
            string email = tbEmail.Text;
            if (fn == "" || ln == "" || addr == "" || city == "" || province == "" || postalCode == "" || country == "" || telephone == "" || memNo == "")
            {
                MessageBox.Show( "Fields couldn't be empty", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (_customer == null)
            {
                _customer = new Customer()
                {
                    FirstName = fn,
                    LastName = ln,
                    Address = addr,
                    City = city,
                    Province = province,
                    PostalCode = postalCode,
                    Country = country,
                    Telephone = telephone,
                    MemberCardNo = memNo,
                    Email = email
                };
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Add(_customer);
                ParentUc.RefreshCustomers();
            }
            else
            {

                _customer.FirstName = fn;
                _customer.LastName = ln;
                _customer.Address = addr;
                _customer.City = city;

                _customer.Province = province;
                _customer.PostalCode = postalCode;
                _customer.Country = country;
                _customer.Telephone = telephone;
                _customer.MemberCardNo = memNo;
                _customer.Email = email;
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.CustomerOperator.Update(_customer);
                ParentUc.RefreshCustomers();

            }
            ClearInputs();
        }
        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            ClearInputs();
            ParentUc.ClearSelection();
        }
    }
}
