﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.customer
{
   
    /// <summary>
    /// Interaction logic for CustomerListView.xaml
    /// </summary>
    public partial class CustomerListView : UserControl
    {
        private List<Customer> customers;
        public event Action<Customer> SelectedCustomerChanged;
        public CustomerListView()
        {
            InitializeComponent();
        }
        public List<Customer> Customers
        {
            set
            {
                customers = value;
                lvCustomers.ItemsSource = customers;
            }
        }
        public Customer SelectedCustomer
        {
            get
            {
                Customer customer = lvCustomers.SelectedItem as Customer;
                return customer;
            }
        }
        public void Search(string searchText)
        {

            if (string.IsNullOrWhiteSpace(searchText))
            {
                lvCustomers.ItemsSource = customers;
            }
            else
            {
                searchText = searchText.ToLower();
                lvCustomers.ItemsSource = (from customer in customers where string.Format("{0} {1}",customer.FirstName,customer.LastName).ToLower().Contains(searchText) || customer.Telephone.ToLower().Contains(searchText) || customer.PostalCode.ToLower().Contains(searchText) select customer).ToList();
            }
        }
        public void ClearSelctionCustomer()
        {
            lvCustomers.UnselectAll();
        }

        private void lvCustomers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Customer customer = lvCustomers.SelectedItem as Customer;
            if (null == customer) return;
            SelectedCustomerChanged?.Invoke(customer);
        }
    }
}
