﻿using EasyModel.business;
using EasySaleDataProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.purchase
{
    /// <summary>
    /// Interaction logic for AddEdit.xaml
    /// </summary>
    public partial class AddEdit : UserControl
    {
        private Purchase _purchase;
        private Commodity _commodity;
        List<Supplier> supplisersList;
        public AddEdit()
        {
            InitializeComponent();
            supplisersList = EasySaleDataFactory.Single.Database.SupplierOperator.FindAll();
            comboSupplier.ItemsSource = supplisersList;
            if (_commodity != null)
            {
                comboComm.IsEditable = false;
                btSearch.IsEnabled = false;
                comboComm.ItemsSource = new List<Commodity> { EasySaleDataFactory.Single.Database.CommodityOperator.Find(_commodity.ID) };
                comboComm.SelectedIndex = 0;
            }
        }
        public Purchase Purchase
        {
            set
            {
                _purchase = value;
                SetInputs();
            }
        }
        public Commodity Commodity
        {
            set
            {
                _commodity = value;
                SetInputs();
            }
        }
        public Content ParentUc { get; set; }
        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            Supplier supplier = (Supplier)comboSupplier.SelectedItem;
            if (supplier == null)
            {
                MessageBox.Show("Please choose supplier", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            int supplierID = supplier.ID;
            string supplierName = supplier.ContactName;
            Commodity comm = (Commodity)comboComm.SelectedItem;
            if (comm == null)
            {
                MessageBox.Show("Please choose commodity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            int commID = comm.ID;
            string commName = comm.Name;

            double price;
            if (!double.TryParse(tbPrice.Text, out price))
            {
                MessageBox.Show("Wrong price", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (price <= 0)
            {
                MessageBox.Show("Wrong price", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double memberPrice = price; // for further use
            double commQty;
            if (!double.TryParse(tbQty.Text, out commQty))
            {
                MessageBox.Show("Wrong quantity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (commQty <= 0)
            {
                MessageBox.Show("Wrong quantity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (_purchase == null)
            {
                btSave.Content = "Add";
                
                _purchase = new Purchase()
                {
                    SupplierID = supplierID,
                    SupplierName = supplierName,
                    CommID = commID,
                    CommName = commName,
                    CommNumber = commQty,
                    Price = price,
                    Total = price * commQty,
                    OrderDate = DateTime.Now,
                    IsPurchased = false
                };
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Add(_purchase);
                //ParentUc.RefreshPurchases();
            }
            else
            {
                btSave.Content = "Update";
                _purchase.SupplierID = supplierID;
                _purchase.SupplierName = supplierName;
                _purchase.CommID = commID;
                _purchase.CommName = commName;
                _purchase.CommNumber = commQty;
                _purchase.Price = price;
                _purchase.Total = price * commQty;
                _purchase.OrderDate = DateTime.Now;
                _purchase.IsPurchased = false;
                EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.Update(_purchase);
                //ParentUc.RefreshPurchases();

            }
            ClearInputs();
        }
        public void SetInputs()
        {
            if (_purchase != null)
            {
                lblId.Content = _purchase.ID;

                tbPrice.Text = _purchase.Price + "";
                int i = 0;
                foreach (var s in supplisersList)
                {
                    if (s.ID == _purchase.SupplierID)
                    {
                        comboSupplier.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                comboComm.IsEditable = false;
                btSearch.IsEnabled = false;
                comboComm.ItemsSource = new List<Commodity> { EasySaleDataFactory.Single.Database.CommodityOperator.Find(_purchase.CommID) };
                comboComm.SelectedIndex = 0;
                tbQty.Text = _purchase.CommNumber + "";
                tbPrice.Text = _purchase.Price + "";
                if (_purchase.IsPurchased)
                {
                    btSave.IsEnabled = false;
                    lblInfo.Visibility = Visibility.Visible;
                }
                else {
                    btSave.IsEnabled = true;
                }
            }
            if (_commodity != null)
            {
                comboComm.IsEditable = false;
                btSearch.IsEnabled = false;
                btSave.IsEnabled = true;
                comboComm.ItemsSource = new List<Commodity> { EasySaleDataFactory.Single.Database.CommodityOperator.Find(_commodity.ID) };
                comboComm.SelectedIndex = 0;
                tbQty.Text = "";
                tbPrice.Text = "";
                tbCommName.Text = _commodity.Name;

            }

        }
        public void ClearInputs()
        {
            _purchase = null;
            lblId.Content = "";
            tbPrice.Text =  "";
            comboSupplier.SelectedIndex = -1;
            comboComm.IsEditable = true;
            btSearch.IsEnabled = true;
            comboComm.ItemsSource = null;
            comboComm.SelectedIndex = 0;
            tbQty.Text = "";
            tbPrice.Text =  "";
            btSave.IsEnabled = true;
            lblInfo.Visibility = Visibility.Hidden;
            
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            ClearInputs();
            ParentUc.ClearSelection();
        }

        private void btSearch_Click(object sender, RoutedEventArgs e)
        {
            string commName = tbCommName.Text;
            try
            {
                //comm = EasySaleDataFactory.Single.Database.CommodityOperator.FindByName(commName).First();
                List<Commodity> commsList = EasySaleDataFactory.Single.Database.CommodityOperator.FindByName(commName);
                if (commsList != null)
                {
                    comboComm.ItemsSource = commsList;
                    comboComm.SelectedItem = commsList.First();
                }
                else
                {
                    MessageBox.Show("Commodity doesn't exist");
                }

            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Commodity doesn't exist");
            }
        }
        private void comboComm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboComm.SelectedItem != null)
            {
                lblUnit.Content = ((Commodity)comboComm.SelectedItem).Unit;
            }
        }
    }
}
