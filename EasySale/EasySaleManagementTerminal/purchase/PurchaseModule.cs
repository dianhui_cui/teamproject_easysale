﻿using EasyModel.ui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace EasySaleManagementTerminal.purchase
{
    class PurchaseModule : ModuleBase
    {
        private ToolBar _toolBar;
        private Content _content;
        public PurchaseModule()
        {
            /*
            _toolBar = new ToolBar();
            _content = new Content();
            _toolBar.AddPurchaseEvent += _content.AddPurchase;
            _toolBar.UpdatePurchaseEvent += _content.UpdatePurchase;
            _toolBar.SearchEvent += _content.SearchPurchase;
            */
        }
        public override string Name
        {
            get
            {
                return "Purchase";
            }
        }
        public override UserControl ToolBar
        {
            get
            {
                if (_toolBar == null)
                {
                    _toolBar = new ToolBar();
                }
                return _toolBar;

            }
        }
        public override UserControl ModuleUI
        {
            get
            {
                if (_content == null)
                {
                    _content = new Content();
                    _toolBar.AddPurchaseEvent += _content.AddPurchase;
                    _toolBar.UpdatePurchaseEvent += _content.UpdatePurchase;
                    _toolBar.SearchEvent += _content.SearchPurchase;
                    
                    _toolBar.CheckInventoryEvent += _content.CheckInventory;
                    _toolBar.FilterCommodityEvent += _content.FilterCommodityByType;
                    _toolBar.ListChangedEvent += _content.ListChanged;
                }
                return _content;
            }

        }
    }
}