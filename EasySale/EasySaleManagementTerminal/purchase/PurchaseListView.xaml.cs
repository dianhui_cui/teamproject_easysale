﻿using EasyModel.business;
using EasySaleDataProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.purchase
{
    /// <summary>
    /// Interaction logic for PurchaseListView.xaml
    /// </summary>
    public partial class PurchaseListView : UserControl
    {
        private List<Purchase> purchases;
        public event Action<Purchase> SelectedPurchaseChanged;
        public PurchaseListView()
        {
            InitializeComponent();
        }
        
        
        public List<Purchase> Purchases
        {
            set
            {
                purchases = value;
                lvPurchases.ItemsSource = purchases;
            }
        }
        public Purchase SelectedPurchase
        {
            get
            {
                Purchase purchase = lvPurchases.SelectedItem as Purchase;
                return purchase;
            }
        }
        public void Search(string searchText)
        {

            if (string.IsNullOrWhiteSpace(searchText))
            {
                lvPurchases.ItemsSource = purchases;
            }
            else
            {
                searchText = searchText.ToLower();
                lvPurchases.ItemsSource = (from purchase in purchases where purchase.CommName.ToLower().Contains(searchText) || purchase.SupplierName.ToLower().Contains(searchText) select purchase).ToList();
            }
        }
        public void ClearSelctionPurchase()
        {
            lvPurchases.UnselectAll();
        }


        private void ListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double width = this.Width;
            
            ColId.Width = width * 0.13;
            ColSupplier.Width = width * 0.13;
            ColCommName.Width = width * 0.13;
            ColCommQty.Width = width * 0.13;
            ColTotal.Width = width * 0.13;
            ColOrderDate.Width = width * 0.13;
            ColIsCompleted.Width = width * 0.13;
            
        }

        private void lvPurchases_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Purchase purchase = lvPurchases.SelectedItem as Purchase;
            if (null == purchase) return;
            SelectedPurchaseChanged?.Invoke(purchase);
        }

        private void btCheckIn_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            Purchase selectedPurchase = btn.DataContext as EasyModel.business.Purchase;
            //selectedPurchase = (Purchase)lvPurchases.SelectedItem;
            //Commodity commodityInPurchase = EasySaleDataFactory.Single.Database.CommodityOperator.Find(selectedPurchase.CommID);
            if (selectedPurchase != null)
            {
                selectedPurchase.IsPurchased = true;
                selectedPurchase.PurchaseDate = DateTime.Now;
                //commodityInPurchase.Quantity += selectedPurchase.CommNumber;
                EasySaleDataFactory.Single.Database.PurchaseOperator.Update(selectedPurchase);
                //EasySaleDataFactory.Single.Database.CommodityOperator.Update(commodityInPurchase);
                this.Purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.FindAll();
            }
        }
    }
}
