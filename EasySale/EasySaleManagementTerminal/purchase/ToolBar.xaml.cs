﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.purchase
{
    /// <summary>
    /// Interaction logic for ToolBar.xaml
    /// </summary>
    public partial class ToolBar : UserControl
    {
        public event Action AddPurchaseEvent;
        public event Action UpdatePurchaseEvent;
        public event Action<string> SearchEvent;
        public event Action<double> CheckInventoryEvent;
        public event Action<int> FilterCommodityEvent;
        public event Action<string> ListChangedEvent;
        public ToolBar()
        {
            InitializeComponent();
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddPurchaseEvent?.Invoke();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            UpdatePurchaseEvent?.Invoke();
        }

        private void tbSearchText_TextChanged(object sender, TextChangedEventArgs e)
        {
            SearchEvent?.Invoke(tbSearchCommodityTypeText.Text);
        }
        
        private void comboCommodityType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboCommodityType.SelectedItem == null) return;
            CommodityType commodityType = comboCommodityType.SelectedItem as CommodityType;
            FilterCommodityEvent?.Invoke(commodityType.TypeCode);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            List<CommodityType> types = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindAll();
            types.Insert(0, new CommodityType() { TypeCode = 0, TypeName = "--All Types--", ParentCode = 0 });
            comboCommodityType.ItemsSource = types;
            comboCommodityType.SelectedIndex = 0;
        }

        private void rbCommodity_Checked(object sender, RoutedEventArgs e)
        {
            rbPurchase.IsChecked = false;
            ListChangedEvent?.Invoke("Commodity");
        }

        private void rbPurchase_Checked(object sender, RoutedEventArgs e)
        {
            rbCommodity.IsChecked = false;
            ListChangedEvent?.Invoke("Purchase");
        }
        
        private void tbInventoryLimit_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbInventoryLimit.Text != "")
            {
                double commQty;
                if (!double.TryParse(tbInventoryLimit.Text, out commQty))
                {
                    MessageBox.Show("Wrong quantity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                CheckInventoryEvent?.Invoke(commQty);
            }
        }
    }
}
