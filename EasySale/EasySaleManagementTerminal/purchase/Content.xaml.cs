﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleManagementTerminal.purchase
{
    /// <summary>
    /// Interaction logic for Content.xaml
    /// </summary>
    public partial class Content : UserControl
    {
        private Dictionary<string, UserControl> subControls = new Dictionary<string, UserControl>();
        public Content()
        {
            InitializeComponent();
        }

        private void DockPanel_Loaded(object sender, RoutedEventArgs e)
        {
            InitContent();
        }
        private void InitContent()
        {
            /*if (!subControls.ContainsKey("list"))
            {
                PurchaseListView lvPurchase = new PurchaseListView();
                lvPurchase.Purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.FindAll();
                lvPurchase.SelectedPurchaseChanged += LvPurchase_SelectedPurchaseChanged;
                docPanel.Children.Add(lvPurchase);
                subControls.Add("list", lvPurchase);
            }*/
            if (!subControls.ContainsKey("listCommodity"))
            {
                CommodityListView lvCommodity = new CommodityListView();
                lvCommodity.Commodities = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindAll();
                lvCommodity.SelectedCommodityChanged += LvCommodity_SelectedCommodityChanged;
                docPanel.Children.Add(lvCommodity);
                subControls.Add("listCommodity", lvCommodity);
            }
        }
        private void LvCommodity_SelectedCommodityChanged(Commodity selectedCommodity)
        {
            AddEdit edit = docPanel.Children.OfType<AddEdit>().FirstOrDefault(); // add/edit purchase for this commodity
            if (null == edit) return;
            edit.Commodity = selectedCommodity;
            List<Purchase> purchasesList = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.FindAll();
            //edit.Purchase = (from purchase in purchasesList where (purchase.CommID == selectedCommodity.ID || purchase.IsPurchased == false) select purchase).First();
        }
        

        private void LvPurchase_SelectedPurchaseChanged(Purchase selectPurchase)
        {
            AddEdit edit = docPanel.Children.OfType<AddEdit>().FirstOrDefault();
            if (null == edit) return;
            edit.Purchase = selectPurchase;
        }

        public void AddPurchase()
        {

            docPanel.Children.Clear();
            if (!subControls.ContainsKey("edit"))
            {
                AddEdit addEdit = new AddEdit();
                addEdit.ParentUc = this;
                subControls.Add("edit", addEdit);
                DockPanel.SetDock(addEdit, Dock.Right);
            }
            (subControls["edit"] as AddEdit).ClearInputs();
            if(null!= (subControls["listCommodity"] as CommodityListView).SelectedCommodity)
                (subControls["edit"] as AddEdit).Commodity = (subControls["listCommodity"] as CommodityListView).SelectedCommodity;
            docPanel.Children.Add(subControls["edit"]);
            docPanel.Children.Add(subControls["listCommodity"]);
          
        }

        public void UpdatePurchase()
        {
            Purchase selectPurchase = (subControls["list"] as PurchaseListView).SelectedPurchase;
            if (selectPurchase == null) return;
            docPanel.Children.Clear();

            if (!subControls.ContainsKey("edit"))
            {
                AddEdit addEdit = new AddEdit();
                addEdit.ParentUc = this;
                subControls.Add("edit", addEdit);
                DockPanel.SetDock(addEdit, Dock.Right);
            }
            (subControls["edit"] as AddEdit).ClearInputs();
            (subControls["edit"] as AddEdit).Purchase = selectPurchase;
            docPanel.Children.Add(subControls["edit"]);
            docPanel.Children.Add(subControls["list"]);
        }
        public void SearchPurchase(string searchText)
        {
            if (docPanel.Children.Count > 1)
            {
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["list"]);
            }
            (subControls["list"] as PurchaseListView).Search(searchText);
        }
        public void RefreshPurchases()
        {
            //lvPurchase.Purchases
            (subControls["list"] as PurchaseListView).Purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.FindAll();
        }
        public void ClearSelection()
        {
            (subControls["list"] as PurchaseListView).ClearSelctionPurchase();
        }
        public void CheckInventory(double commQty)
        {
            CommodityListView listCommodity = docPanel.Children.OfType<CommodityListView>().FirstOrDefault();
            if (null == listCommodity)
            {
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["listCommodity"]);
            }
            else
            {

                if (docPanel.Children.Count > 1)
                {
                    docPanel.Children.Clear();
                    docPanel.Children.Add(subControls["listCommodity"]);
                }
            }
            (subControls["listCommodity"] as CommodityListView).Search(commQty);
        }
        public void FilterCommodityByType(int commodityType)
        {
            if (!subControls.ContainsKey("listCommodity")) return;
            CommodityListView listCommodity = docPanel.Children.OfType<CommodityListView>().FirstOrDefault();
            if (null == listCommodity)
            {
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["listCommodity"]);
            }
            else
            {

                if (docPanel.Children.Count > 1)
                {
                    docPanel.Children.Clear();
                    docPanel.Children.Add(subControls["listCommodity"]);
                }
            }
          (subControls["listCommodity"] as CommodityListView).FilterByCommodityType(commodityType);
        }

        public void RefreshCommodity()
        {
            //lvCustomer.Customers
            (subControls["listCommodity"] as CommodityListView).Commodities = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindAll();
        }

        public void ClearCommoditySelection()
        {
            (subControls["listCommodity"] as CommodityListView).ClearSelctionCommodity();
        }
        public void ListChanged(string listName)
        {
            if (listName == "Commodity")
            {
                if (!subControls.ContainsKey("listCommodity"))
                {
                    CommodityListView lvCommodity = new CommodityListView();
                    lvCommodity.Commodities = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindAll();
                    lvCommodity.SelectedCommodityChanged += LvCommodity_SelectedCommodityChanged;
                    docPanel.Children.Add(lvCommodity);
                    subControls.Add("listCommodity", lvCommodity);
                }
                else {

                    (subControls["listCommodity"] as CommodityListView).Commodities = EasySaleDataProvider.EasySaleDataFactory.Single.Database.CommodityOperator.FindAll();
                }
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["listCommodity"]);
            }
            else
            {
                docPanel.Children.Clear();

                if (!subControls.ContainsKey("list"))
                {
                    PurchaseListView lvPurchase = new PurchaseListView();
                    lvPurchase.Purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.FindAll();
                    lvPurchase.SelectedPurchaseChanged += LvPurchase_SelectedPurchaseChanged;
                    docPanel.Children.Add(lvPurchase);
                    subControls.Add("list", lvPurchase);
                }
                else
                {
                    (subControls["list"] as PurchaseListView).Purchases = EasySaleDataProvider.EasySaleDataFactory.Single.Database.PurchaseOperator.FindAll();

                }
                docPanel.Children.Clear();
                docPanel.Children.Add(subControls["list"]);
            }
        }
    }
}
