﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyModel.database
{
    public interface ICommodity
    {
        void Add(Commodity commodity);
       
        void Update(Commodity commodity);
        void SetPicture(int id, byte[] picture);
        byte[] GetPicture(int id);

        List<Commodity> Find(CommodityType type);
        List<Commodity> FindAll();
        Commodity Find(int id);
        Commodity FindByBarcode(string barcode);
        Commodity FindByCommCode(int commCode);
        List<Commodity> FindByName(string name);
        

    }
}
