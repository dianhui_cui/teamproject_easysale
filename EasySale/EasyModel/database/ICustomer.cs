﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
namespace EasyModel.database
{
    public interface ICustomer
    {
        void Add(Customer customer);
        void Update(Customer customer);
        Customer Find(int id);
        Customer Find(string MemberCardNo);
        List<Customer> FindAll();
        
        
    }
}
