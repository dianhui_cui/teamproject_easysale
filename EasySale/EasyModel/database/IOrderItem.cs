﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
namespace EasyModel.database
{
    public interface IOrderItem
    {
        void Add(OrderItem orderItem);

        List<OrderItem> FindByOrder(Order order);
        List<OrderItem> FindByOrderID(int orderID);

    }
}
