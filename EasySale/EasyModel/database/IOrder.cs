﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
namespace EasyModel.database
{
    public interface IOrder
    {
        void Add(Order order);
        void Add(Order order,List<OrderItem> items);
        void Update(Order order);

        Order Find(int id);
        List<Order> FindByCustomer(Customer customer);
        List<Order> FindByCustomerID(int custID);
        List<Order> FindByTime(DateTime start, DateTime end);

    }
}
