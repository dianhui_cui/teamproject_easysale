﻿using EasyModel.business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyModel.database
{
    public interface ICommodityType
    {
        List<CommodityType> FindByParent(int parentCode);
        List<CommodityType> FindAll();
        CommodityType Find(int code);
        void Add(CommodityType commodityType);
        void Update(CommodityType commodityType);
    }
}
