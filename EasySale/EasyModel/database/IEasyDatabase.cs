﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyModel.database
{
    public interface IEasyDatabase
    {
        IOrder OrderOperator { get; set; }
        IOrderItem OrderItemOperator { get; set; }
        ISupplier SupplierOperator { get; set; }
        ICustomer CustomerOperator { get; set; }
        IPurchase PurchaseOperator { get; set; }
        ICommodity CommodityOperator { get; set; }
        ICommodityType CommodityTypeOperator { get; set; }
        void Close();
    }
}
