﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
namespace EasyModel.database
{
    public interface IPurchase
    {
        void Add(Purchase purchase);
        void Add(List<Purchase> purchases);
        void Update(Purchase purchase);
        Purchase Find(int id);
        List<Purchase> findBySupplier(int supplierID);
        List<Purchase> findBySupplier(int supplierID, bool isCompleted);
        List<Purchase> FindAll(bool isCompleted);
        List<Purchase> FindAll();
    }
}
