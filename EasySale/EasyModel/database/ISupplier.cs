﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
namespace EasyModel.database
{
    public interface ISupplier
    {
        void Add(Supplier supplier);
        void Update(Supplier supplier);
        Supplier Find(int id);
        List<Supplier> FindAll();
    }
}
