﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace EasyModel.utility
{
    public class Utility
    {
        private static ILog _log = LogManager.GetLogger(typeof(Utility));
        public static object ReflectClass(String className)
        {
            try
            {
                _log.Warn(string.Format("begin ReflectClass[{0}]", className));
                Type type = null;
                try
                {
                    type = Type.GetType(className);
                    if (null == type)
                    {
                        throw new ArgumentException(className);
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(ex.Message, ex);
                }

                Type[] paramTypes = System.Type.EmptyTypes;
                ConstructorInfo consInfo = type.GetConstructor(paramTypes);
                object[] paramArray = new object[0];
                return consInfo.Invoke(paramArray);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                _log.Error(ex.StackTrace, ex);
            }
            return null;

        }
        public static byte[] ImageSourceToBytes(ImageSource imageSource)
        {
            byte[] bytes = null;
            var bitmapSource = imageSource as BitmapSource;

            if (bitmapSource != null)
            {
                BitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            return bytes;


        }
        public static ImageSource ByteArrayToImage(byte[] byteArrayIn)
        {
            if (null==byteArrayIn|| 0 == byteArrayIn.Length) return null;
            ImageSource result;
            using (var stream = new MemoryStream(byteArrayIn))
            {
                result = BitmapFrame.Create(
                    stream, BitmapCreateOptions.None, BitmapCacheOption.OnLoad);
            }
            return result;

        }

    }
}
