﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyModel.utility
{
    public class ModuleConfigSection: ConfigurationSection
    {
        [ConfigurationProperty("", IsRequired = true, IsDefaultCollection = true)]
        public ModuleConfigInstanceCollection Instances
        {
            get { return (ModuleConfigInstanceCollection)this[""]; }
            set { this[""] = value; }
        }
    }
    public class ModuleConfigInstanceCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ModuleConfigInstanceElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            //set to whatever Element Property you want to use for a key
            return ((ModuleConfigInstanceElement)element).Name;
        }
    }

    public class ModuleConfigInstanceElement : ConfigurationElement
    {
        //Make sure to set IsKey=true for property exposed as the GetElementKey above
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)base["name"]; }
            set { base["name"] = value; }
        }

        [ConfigurationProperty("moudle", IsRequired = true)]
        public string Moudle
        {
            get { return (string)base["moudle"]; }
            set { base["moudle"] = value; }
        }
    }
    public class ModuleLoader
    {
        public static List<string> GetClientMoudles()
        {
            List<string> moudles = new List<string>();
            var config = ConfigurationManager.GetSection("moudles")
                     as ModuleConfigSection;

         
            foreach (ModuleConfigInstanceElement e in config.Instances)
            {
                moudles.Add(e.Moudle);
            }
            return moudles;

        }
    }

}
