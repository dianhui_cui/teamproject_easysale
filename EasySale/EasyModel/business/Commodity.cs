﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyModel.business
{
    public class Commodity
    {
        public Commodity()
        { 
        
        }
        public Commodity( string barcode, int commCode, string name, double qty, double price)
        {
            Barcode = barcode;
            CommCode = commCode;
            Name = name;
            Price = price;
            Quantity = qty;
        }
        public int ID { get; set; }
        public string Barcode { get; set; }
        public int CommCode { get; set; }
        public int CommType { get; set; }
        public string CommTypeName { get; set; }
        
        //public CommodityType CommodityType { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public double MemberPrice { get; set; }
        public double Quantity { get; set; }
    
        public CommodityUnit Unit { get; set; }
        public override string ToString()
        {
            return string.Format("{0}: ${1:0.00} per {2}", Name, Price, Unit);
        }
    }
}
