﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyModel.business
{
    public class CommodityType
    {
        public CommodityType() { }
        public int TypeCode { get; set; }
        public int ParentCode { get; set; }
        //public CommodityType Parent { get; set; }

        public string TypeName { get; set; }

        public double GST { get; set; }

        public double PST { get; set; }
        public override string ToString()
        {
            return this.TypeName;

        }
    }
}
