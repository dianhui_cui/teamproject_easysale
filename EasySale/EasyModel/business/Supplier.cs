﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyModel.business
{
    public class Supplier
    {
        public Supplier() { }
        public int ID { get; set; }
        public string Company { get; set; }
        public string ContactName {get;set;}
        public string Address { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string ProductTypes { get; set; }
        public string Memo { get; set; }
        public override string ToString()
        {
            return this.Company;
        }
    }
}
