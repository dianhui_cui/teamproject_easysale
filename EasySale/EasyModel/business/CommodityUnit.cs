﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyModel.business
{
    public enum CommodityUnit
    {
        lb, kg, ea, pc, ft, gal
    }
}
