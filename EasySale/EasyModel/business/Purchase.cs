﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyModel.business
{
    public class Purchase
    {
        public Purchase() { }
        public int ID { get; set; }
        public int SupplierID { get; set; }
        public string SupplierName { get; set; }
        public int CommID { get; set; }
        public string CommName { get; set; }

        //public virtual Supplier Supplier { get; set; }
        //public virtual Commodity Commodity { get; set; }
        public double CommNumber { get; set; }
        public double Price { get; set; }
        public double Total { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public bool IsPurchased { get; set; }
    }
}
