﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace EasyModel.business
{
    public class Order
    {
        public Order() { }
        public int ID { get; set; }
        public int CustomerID { get; set; }
        public string CustomerFullName { get; set; }
        //public Customer Customer { get; set; }
        public DateTime OrderTime { get; set; }
        public double Total { get; set; }
    }
}
