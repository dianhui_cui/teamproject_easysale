﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace EasyModel.business
{
    public class OrderItem
    {
        public OrderItem() { }
        public int ID { get; set; }
        public int OrderID { get; set; }

        //public virtual Order Order { get; set; }
        public int CommID { get; set; }
        public string CommName { get; set; }
        //public virtual Commodity Commodity { get; set; }

        public double ItemCount { get; set; }

        public double UnitPrice { get; set; }
        public double SubTotal { get; set; }
        public double GST { get; set; }

        public double PST { get; set; }
    }
}
