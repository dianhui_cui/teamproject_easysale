﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using EasySaleDataProvider;
using EasyModel.business;
using Microsoft.Win32;
using System.IO;
//using System.Data.Entity;
namespace EasySaleTerminal
{
    /// <summary>
    /// Interaction logic for AddEditProductDialog.xaml
    /// </summary>
    public partial class AddEditProductDialog : Window
    {
        // enum Unit { ea, pc, ft, lb, gal };
        Commodity CurrentComm;
        public AddEditProductDialog(Commodity comm)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            comboUnit.ItemsSource = Enum.GetValues(typeof(EasyModel.business.CommodityUnit));
            List<CommodityType> typeList = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(0);
            comboCategory.ItemsSource = typeList;
            CurrentComm = comm;
            if (CurrentComm == null)
            {
                comboCategory.SelectedItem = typeList.First();
                comboSubCategory.ItemsSource = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(((CommodityType)comboCategory.SelectedItem).TypeCode);
                btSave.Content = "Add";
            } else
            {
                btSave.Content = "Update";
                CommodityType ct = EasySaleDataFactory.Single.Database.CommodityTypeOperator.Find(CurrentComm.CommType);
                
                //List<CommodityType> ctList = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(0);
                int i = 0;
                foreach (var c in typeList)
                {
                    if (c.TypeCode == ct.ParentCode)
                    {
                        comboCategory.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                //comboCategory.SelectedItem = EasySaleDataFactory.Single.Database.CommodityTypeOperator.Find(ct.ParentCode); 
                //comboCategory.SelectedItem = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(0);

                List<CommodityType> subCTList = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(ct.ParentCode);
                comboSubCategory.ItemsSource = subCTList;// ((CommodityType)comboCategory.SelectedItem).TypeCode);
                int j = 0;
                foreach (var c in subCTList)
                {
                    if (c.TypeCode == ct.TypeCode)
                    {
                        comboSubCategory.SelectedIndex = j;
                        break;
                    }
                    j++;
                }
                //comboSubCategory.SelectedItem = from sct in subCTList where sct.TypeCode==CurrentComm.CommType select sct ;
                lblId.Content = CurrentComm.ID;
                tbBarcode.Text = CurrentComm.Barcode?? CurrentComm.Barcode;
                tbCommCode.Text = CurrentComm.CommCode+"";
                tbName.Text = CurrentComm.Name;
                tbPrice.Text = CurrentComm.Price + "";
                int k = 0;
                foreach (var c in Enum.GetValues(typeof(EasyModel.business.CommodityUnit)))
                {
                    if (c.ToString() == comm.Unit.ToString())
                    {
                        comboUnit.SelectedIndex = k;
                        break;
                    }
                    k++;
                }
                var image = new BitmapImage();
                if (CurrentComm.Picture.Length != 0)
                {
                    using (var mem = new MemoryStream(CurrentComm.Picture))
                    {
                        mem.Position = 0;
                        image.BeginInit();
                        image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.UriSource = null;
                        image.StreamSource = mem;
                        image.EndInit();
                    }
                    imgPhoto.Source = image;
                }
            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            string barcode = tbBarcode.Text ;
            //int commType;
            //int.TryParse(comboCommType.Text, out commType) ;//ComboBox, using commTypeName in database as itemsource

            //string commTypeName = EasySaleDataFactory.Single.Database.CommodityTypeOperator.Find(commType).TypeName; //from ct in commtypedatabase where ct.commTypeName=commTypeName select ct.commType;
            CommodityType ct = (CommodityType)comboSubCategory.SelectedItem;
            
            if (ct == null)
            {
                MessageBox.Show(this, "Please select a sub category", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            int commType = ct.TypeCode;
            string commTypeName = ct.TypeName;
            string name = tbName.Text;
            double price;
            if (!double.TryParse(tbPrice.Text, out price))
            {
                MessageBox.Show(this, "Wrong price", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (price <= 0)
            {
                MessageBox.Show(this, "Wrong price", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double memberPrice = price; // for further use
            int commCode;
            if (!int.TryParse(tbCommCode.Text, out commCode))
            {
                MessageBox.Show(this, "Wrong commCode", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            /*double quantity;
            if (!double.TryParse(tbQty.Text, out quantity))
            {
                MessageBox.Show(this, "Wrong quantity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (quantity <= 0)
            {
                MessageBox.Show(this, "Wrong quantity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }*/
            byte[] picture;
            if (imgPhoto.Source == null)
            {
                picture = new byte[0];
            } else {
                picture = bitmapImageToByteArray((BitmapImage)imgPhoto.Source);
            }
            if (comboUnit.SelectedIndex == -1)
            {
                MessageBox.Show(this, "Please choose the unit", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            CommodityUnit unit = (CommodityUnit)comboUnit.SelectedItem;
            //try
            //{
            if (CurrentComm == null)
            {
                EasySaleDataFactory.Single.Database.CommodityOperator.Add(new Commodity { CommCode = commCode, Barcode = barcode, CommType = commType, CommTypeName = commTypeName, MemberPrice = memberPrice, Name = name, Picture = picture, Price = price, Unit = unit });
            } else
            {
                CurrentComm.CommCode = commCode;
                CurrentComm.Barcode = barcode;
                CurrentComm.CommType = commType;
                CurrentComm.CommTypeName = commTypeName;
                CurrentComm.MemberPrice = memberPrice;
                CurrentComm.Name = name;
                CurrentComm.Picture = picture;
                CurrentComm.Price = price;
                CurrentComm.Unit = unit;
            EasySaleDataFactory.Single.Database.CommodityOperator.Update(CurrentComm);
            }
            /*} catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                MessageBox.Show();
            }*/
            DialogResult = true;
        }
        private byte[] bitmapImageToByteArray(BitmapImage bitmapImage)
        {
            if (bitmapImage == null) return null;
            byte[] data;
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                data = ms.ToArray();
            }
            return data;
        }
        private void btAddPhoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "images |*.gif;*.png;*.jpg|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    // imageInBytes = File.ReadAllBytes(openFileDialog.FileName);
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(openFileDialog.FileName);
                    bitmap.EndInit();
                    imgPhoto.Source = bitmap;
                }
                catch (IOException ex)
                {
                    MessageBox.Show(this, "Error Reading file", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                catch (UriFormatException ex)
                {
                    MessageBox.Show(this, "Error Reading file", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void comboCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboCategory.SelectedItem != null)
            {
                comboSubCategory.ItemsSource = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(((CommodityType)comboCategory.SelectedItem).TypeCode);
            }
        }
    }
}
