﻿using EasyModel.business;
using EasySaleDataProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySaleTerminal
{
    /// <summary>
    /// Interaction logic for AddEditCustomer.xaml
    /// </summary>
    public partial class AddEditCustomer : Window
    {
        // usually enum is not for purpose of display, 
        // a better way to display province/States list will be 
        // Country / State / Province Lookup Table
        // enum Province { Alberta, BritishColumbia, Manitoba, NewBrunswick, NewfoundlandLabrado, NorthwestTerritories, NovaScotia, Nunavut, Ontario, PrinceEdwardIsland, Quebec, Saskatchewan, Yukon};
        Customer Currentcustomer;
        public AddEditCustomer(Customer customer)
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            Currentcustomer = customer;
            if (Currentcustomer == null)
            {                
                btSave.Content = "Add";
            }
            else
            {
                btSave.Content = "Update";
                tbFirstName.Text = Currentcustomer.FirstName;
                tbLastName.Text  = Currentcustomer.LastName;
                tbAddress.Text   = Currentcustomer.Address;
                tbCity.Text      = Currentcustomer.City;
                
                tbProvince.Text  = Currentcustomer.Province;
                tbPostalCode.Text= Currentcustomer.PostalCode;
                tbCountry.Text   = Currentcustomer.Country;
                tbTelephone.Text = Currentcustomer.Telephone;
                tbMemberCardNo.Text = Currentcustomer.MemberCardNo;
                tbEmail.Text = Currentcustomer.Email;

            }
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            string fn = tbFirstName.Text;
            string ln = tbLastName.Text;
            string addr = tbAddress.Text;
            string city = tbCity.Text;
            string province = tbProvince.Text;
            string postalCode = tbPostalCode.Text;
            string country = tbCountry.Text;
            string telephone = tbTelephone.Text;
            string memNo = tbMemberCardNo.Text;
            string email = tbEmail.Text;
            //int commType;
            //int.TryParse(comboCommType.Text, out commType) ;//ComboBox, using commTypeName in database as itemsource

            //string commTypeName = EasySaleDataFactory.Single.Database.CommodityTypeOperator.Find(commType).TypeName; //from ct in commtypedatabase where ct.commTypeName=commTypeName select ct.commType;
            
            if (fn=="" || ln == "" || addr == "" || city == "" || province == "" || postalCode == "" || country == "" || telephone == "" || memNo == "")
            {
                MessageBox.Show(this, "Fields couldn't be empty", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            if (Currentcustomer == null)
            {
                EasySaleDataFactory.Single.Database.CustomerOperator.Add(new Customer { FirstName = fn, LastName = ln, Address = addr, City = city, Province = province, PostalCode = postalCode, Country = country, Telephone = telephone, MemberCardNo = memNo, Email = email});
            }
            else
            {
                Currentcustomer.FirstName = fn;
                Currentcustomer.LastName  = ln;
                Currentcustomer.Address   = addr;
                Currentcustomer.City      = city;

                Currentcustomer.Province  = province;
                Currentcustomer.PostalCode= postalCode;
                Currentcustomer.Country   = country;
                Currentcustomer.Telephone = telephone;
                Currentcustomer.MemberCardNo= memNo;
                Currentcustomer.Email = email;
                EasySaleDataFactory.Single.Database.CustomerOperator.Update(Currentcustomer);
            }
            DialogResult = true;
        }
    }
}
