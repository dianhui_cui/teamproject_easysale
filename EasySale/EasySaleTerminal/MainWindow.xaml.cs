﻿using EasyModel.business;
using EasySaleDataProvider;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EasySaleTerminal
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ILog log = log4net.LogManager.GetLogger(typeof(MainWindow));
        //Commodity CurrentCommodity;
        public MainWindow()
        {
            InitializeComponent();
            InitCommCategory();
            LoadCommListFromDB();
            LoadCustomersListFromDB();
            LoadPurchasesListFromDB();
        }

        void InitCommCategory()
        {
            List<CommodityType> typeList = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(0);
            lvCategory.ItemsSource = typeList;
            lvCategory.SelectedIndex = 0;
            lvSubCategory.ItemsSource = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(((CommodityType)lvCategory.SelectedItem).TypeCode);
            lvSubCategory.SelectedIndex = 0;
        }
        void LoadCommListFromDB()
        {
            lvCommList.ItemsSource = EasySaleDataFactory.Single.Database.CommodityOperator.Find(((CommodityType)lvSubCategory.SelectedItem)); 
        }
        void LoadCustomersListFromDB()
        {
            lvCustomers.ItemsSource = EasySaleDataFactory.Single.Database.CustomerOperator.FindAll();
        }

        void LoadPurchasesListFromDB()
        {
            lvPurchases.ItemsSource = EasySaleDataFactory.Single.Database.PurchaseOperator.FindAll();
        }

        private void btManageCommodities_Click(object sender, RoutedEventArgs e)
        {
            AddEditProductDialog dlg = new AddEditProductDialog(null);
            if (dlg.ShowDialog() == true)
            {
                LoadCommListFromDB();
            }
        }

        private void lvCommList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Commodity selectedComm =(Commodity)lvCommList.SelectedItem;
            if (selectedComm != null)
            {
                AddEditProductDialog dlg = new AddEditProductDialog(selectedComm);
                if (dlg.ShowDialog() == true)
                {
                    LoadCommListFromDB();
                }
            }
        }

        private void btAddEditCustomer_Click(object sender, RoutedEventArgs e)
        {
            AddEditCustomer dlg = new AddEditCustomer(null);
            if (dlg.ShowDialog() == true)
            {
                LoadCustomersListFromDB();
            }
        }

        private void lvCustomers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Customer selectedCustomer = (Customer)lvCustomers.SelectedItem;
            if (selectedCustomer != null)
            {
                AddEditCustomer dlg = new AddEditCustomer(selectedCustomer);
                if (dlg.ShowDialog() == true)
                {
                    LoadCustomersListFromDB();
                }
            }
        }

        private void btAddEditPurchase_Click(object sender, RoutedEventArgs e)
        {
            AddEditPurchase dlg = new AddEditPurchase(null);
            if (dlg.ShowDialog() == true)
            {
                LoadPurchasesListFromDB();
            }
        }

        private void lvPurchases_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Purchase selectedPurchase = (Purchase)lvPurchases.SelectedItem;
            if (selectedPurchase != null)
            {
                AddEditPurchase dlg = new AddEditPurchase(selectedPurchase);
                if (dlg.ShowDialog() == true)
                {
                    LoadPurchasesListFromDB();
                    LoadCommListFromDB();
                }
            }
        }

        
        private void miContextSetToCompleted_Click(object sender, RoutedEventArgs e)
        {
            Purchase selectedPurchase = (Purchase)lvPurchases.SelectedItem;
            Commodity commodityInPurchase = EasySaleDataFactory.Single.Database.CommodityOperator.Find(selectedPurchase.CommID);
            if (selectedPurchase != null)
            {
                selectedPurchase.IsPurchased = true;
                selectedPurchase.PurchaseDate = DateTime.Now;
                commodityInPurchase.Quantity += selectedPurchase.CommNumber;
                EasySaleDataFactory.Single.Database.PurchaseOperator.Update(selectedPurchase);
                EasySaleDataFactory.Single.Database.CommodityOperator.Update(commodityInPurchase);
                LoadPurchasesListFromDB();
                LoadCommListFromDB();
            }
        }

        private void lvPurchases_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            ContextMenu cm = ctxMenu as ContextMenu;
        }

        private void lvCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvCategory.SelectedItem != null)
            {
                lvSubCategory.ItemsSource = EasySaleDataFactory.Single.Database.CommodityTypeOperator.FindByParent(((CommodityType)lvCategory.SelectedItem).TypeCode);
                lvSubCategory.SelectedIndex = 0;
            }
        }

        private void lvSubCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvSubCategory.SelectedItem != null)
            {
                LoadCommListFromDB();
            }
        }

        private void miContextPurchaseCommodity_Click(object sender, RoutedEventArgs e)
        {
            Commodity selectedComm = (Commodity)lvCommList.SelectedItem;
            if (selectedComm != null)
            {
                AddEditPurchase dlg = new AddEditPurchase(null);
                dlg.comboComm.IsEditable = false;
                dlg.btSearch.IsEnabled = false;
                dlg.comboComm.ItemsSource = new List<Commodity> { EasySaleDataFactory.Single.Database.CommodityOperator.Find(selectedComm.ID) };
                dlg.comboComm.SelectedIndex = 0;
                if (dlg.ShowDialog() == true)
                {
                    LoadPurchasesListFromDB();
                }
            }
        }

        private void ctxCommMenu_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            ContextMenu cm = ctxCommMenu as ContextMenu;
        }

        private void btCheckInventory_Click(object sender, RoutedEventArgs e)
        {
            double commQty;
            if (!double.TryParse(tbInventoryLimit.Text, out commQty))
            {
                MessageBox.Show(this, "Wrong quantity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            List<Commodity> commList = EasySaleDataFactory.Single.Database.CommodityOperator.Find(((CommodityType)lvSubCategory.SelectedItem));
            lvCommList.ItemsSource = from c in commList where c.Quantity <= commQty select c;
        }
    }
}
