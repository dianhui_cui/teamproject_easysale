﻿using EasyModel.business;
using EasySaleDataProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EasySaleTerminal
{
    /// <summary>
    /// Interaction logic for AddEditPurchase.xaml
    /// </summary>
    public partial class AddEditPurchase : Window
    {
        Purchase CurrentPurchase;
        public AddEditPurchase(Purchase purchase)
        {
            InitializeComponent();
            //comboComm.IsEditable = true;
            CurrentPurchase = purchase;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            List<Supplier> supplisersList = EasySaleDataFactory.Single.Database.SupplierOperator.FindAll();
            comboSupplier.ItemsSource = supplisersList;
            CurrentPurchase = purchase;
            if (CurrentPurchase == null)
            {
                btSave.Content = "Add";
            }
            else
            {
                btSave.Content = "Update";
                lblId.Content = CurrentPurchase.ID;
                tbPrice.Text = CurrentPurchase.Price + "";
                int i = 0;
                foreach (var s in supplisersList)
                {
                    if (s.ID == CurrentPurchase.SupplierID)
                    {
                        comboSupplier.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                comboComm.IsEditable = false;
                btSearch.IsEnabled = false;
                comboComm.ItemsSource = new List<Commodity> { EasySaleDataFactory.Single.Database.CommodityOperator.Find(CurrentPurchase.CommID)};
                comboComm.SelectedIndex = 0;
                tbQty.Text = CurrentPurchase.CommNumber + "";
                tbPrice.Text = CurrentPurchase.Price + "";
                if (CurrentPurchase.IsPurchased) {
                    btSave.IsEnabled = false;
                    lblInfo.Visibility = Visibility.Visible;
                }
            }
        }

        private void btSearch_Click(object sender, RoutedEventArgs e)
        {
            string commName = tbCommName.Text;
            try
            {
                //comm = EasySaleDataFactory.Single.Database.CommodityOperator.FindByName(commName).First();
                List<Commodity> commsList = EasySaleDataFactory.Single.Database.CommodityOperator.FindByName(commName);
                if (commsList != null)
                {
                    comboComm.ItemsSource = commsList;
                    comboComm.SelectedItem = commsList.First();
                }
                else
                {
                    MessageBox.Show("Commodity doesn't exist");
                }
                
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Commodity doesn't exist");
            }
        }
        
        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            int supplierID = ((Supplier)comboSupplier.SelectedItem).ID;
            string supplierName = ((Supplier)comboSupplier.SelectedItem).ContactName;
            int commID = ((Commodity)comboComm.SelectedItem).ID;
            string commName = ((Commodity)comboComm.SelectedItem).Name;

            double price;
            if (!double.TryParse(tbPrice.Text, out price))
            {
                MessageBox.Show(this, "Wrong price", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (price <= 0)
            {
                MessageBox.Show(this, "Wrong price", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double memberPrice = price; // for further use
            double commQty;
            if (!double.TryParse(tbQty.Text, out commQty))
            {
                MessageBox.Show(this, "Wrong quantity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (commQty <= 0)
            {
                MessageBox.Show(this, "Wrong quantity", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (CurrentPurchase == null)
            {
                MessageBox.Show(this, string.Format("Adding {0} Price ${1}, Total ${2}",commName, price, price * commQty), "Input info", MessageBoxButton.OK, MessageBoxImage.Information);
                EasySaleDataFactory.Single.Database.PurchaseOperator.Add(new Purchase { SupplierID = supplierID, SupplierName = supplierName, CommID = commID, CommName = commName, CommNumber = commQty, Price = price, Total = price * commQty, OrderDate = DateTime.Now, IsPurchased = false });
            }
            else
            {
                CurrentPurchase.SupplierID = supplierID;
                CurrentPurchase.SupplierName = supplierName;
                CurrentPurchase.CommID = commID;
                CurrentPurchase.CommName = commName;
                CurrentPurchase.CommNumber = commQty;
                CurrentPurchase.Price = price;
                CurrentPurchase.Total = price * commQty;
                CurrentPurchase.OrderDate = DateTime.Now;
                CurrentPurchase.IsPurchased = false;


                EasySaleDataFactory.Single.Database.PurchaseOperator.Update(CurrentPurchase);
            }
            
            DialogResult = true;
        }

        private void comboComm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboComm.SelectedItem != null)
            {
                lblUnit.Content = ((Commodity)comboComm.SelectedItem).Unit;
            }
        }
    }
}
