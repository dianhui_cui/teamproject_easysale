﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfAppTest.supplier
{
    public class SupplierModule : ModuleBase
    {
        private ToolBar _toolBar;
        private Content _content;
        public SupplierModule()
        {
            _toolBar = new ToolBar();
            _content = new Content();
            _toolBar.AddSupplierEvent += _content.AddSupplier;
            _toolBar.UpdateSupplierEvent += _content.UpdateSupplier;
            _toolBar.SearchEvent += _content.SearchSupplier;
        }
        public override string Name
        {
            get
            {
                return "Supplier";
            }
        }
        public override UserControl ToolBar { get { return _toolBar; }  }
        public override UserControl ModuleUI { get { return _content; } }
    }
}
