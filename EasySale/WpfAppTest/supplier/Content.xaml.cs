﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppTest.supplier
{
    /// <summary>
    /// Interaction logic for Content.xaml
    /// </summary>
    public partial class Content : UserControl
    {
        SupplierListView lvSupplier;
        public Content()
        {
            InitializeComponent();
        }

        private void DockPanel_Loaded(object sender, RoutedEventArgs e)
        {
            InitContent();
        }
        private void InitContent()
        {
            lvSupplier = new SupplierListView();
            docPanel.Children.Add(lvSupplier);
        }
        public void AddSupplier()
        {
            if (docPanel.Children.Count == 1 )
            {
                docPanel.Children.Clear();
                AddEdit addEdit = new AddEdit();
                DockPanel.SetDock(addEdit, Dock.Right);
                docPanel.Children.Add(addEdit);
                docPanel.Children.Add(lvSupplier);


            }
        }

        public void UpdateSupplier()
        {
            if (docPanel.Children.Count == 1)
            {
                docPanel.Children.Clear();
                AddEdit addEdit = new AddEdit();
                DockPanel.SetDock(addEdit, Dock.Right);
                docPanel.Children.Add(addEdit);
                docPanel.Children.Add(lvSupplier);

            }
        }
        public void SearchSupplier(string searchText)
        {
            if (docPanel.Children.Count > 1)
            {
                docPanel.Children.Clear();
                docPanel.Children.Add(lvSupplier);
            }
        }
    }
}
