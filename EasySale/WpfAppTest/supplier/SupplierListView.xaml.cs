﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppTest.supplier
{
    /// <summary>
    /// Interaction logic for SupplierListView.xaml
    /// </summary>
    public partial class SupplierListView : UserControl
    {
        public SupplierListView()
        {
            InitializeComponent();
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {



        }

        private void ListView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            double width = this.Width;
            colCompany.Width = width * 0.1;
            colContact.Width = width * 0.1;
            colTelephone.Width = width * 0.1;
            colProducts.Width = width * 0.1;
            colEmail.Width = width * 0.1;
            colAddress.Width = width * 0.1;
            colCity.Width = width * 0.1;
            colProvince.Width = width * 0.1;
            colCountry.Width = width * 0.1;
            colPostalCode.Width = width * 0.1;
        }
    }
}
