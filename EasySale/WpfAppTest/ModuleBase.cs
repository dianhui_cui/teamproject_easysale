﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfAppTest
{
    public abstract  class ModuleBase
    {
        public abstract string Name { get; }
        public abstract UserControl ToolBar { get; }
        public abstract UserControl ModuleUI { get; }
    }
}
