﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfAppTest
{
    /// <summary>
    /// Interaction logic for ESMain.xaml
    /// </summary>
    public partial class ESMain : Window
    {

        public ESMain()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitMoudles();
        }
        private void InitMoudles()
        {
            List<string> moudleAssemblies = ModuleLoader.GetClientMoudles();
            List<ModuleBase> modules = new List<ModuleBase>();
            foreach (var item in moudleAssemblies)
            {

                ModuleBase module = Utility.ReflectClass(item) as ModuleBase;
                if (null != module)
                {
                    modules.Add(module);
                }
            }
           
            foreach (var module in modules)
            {
                //add toobar
                StackPanel stackPanel = new StackPanel();
                stackPanel.Children.Add(module.ToolBar);
                //Grid grid = new Grid();
                //grid.Children.Add(module.ToolBar);
                
                TabItem tab = new TabItem();
                tab.Tag = module;
                tab.Header = module.Name;
                tab.Content = stackPanel;
                tabToolbar.Items.Add(tab);
                //add content
                if (ucContainer.Children.Count == 0)
                {
                    tabToolbar.SelectedIndex = 0;
                    //ucContainer.Children.Add(module.ModuleUI);
                }
            }
        }

        private void tabToolbar_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tabToolbar.SelectedIndex == -1) return;
            ModuleBase selectModule = (tabToolbar.SelectedItem as TabItem).Tag as ModuleBase;
            ucContainer.Children.Clear();
            ucContainer.Children.Add(selectModule.ModuleUI);

        }
    }
}
