﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace EasySaleDbMysqlFirst
{
    public class DBHelp
    {
        private static ILog _log = LogManager.GetLogger(typeof(DBHelp));
        /// <summary>
        /// get Entity Framework connection string
        /// </summary>
        /// <param name="ModelName"></param>
        /// <returns></returns>
        public static string GetEFConnectionString(string ModelName)
        {
            try
            {
                string xPath = string.Format("/configuration/connectionStrings//add[@name='{0}']", ModelName);
                XmlDocument doc = new XmlDocument();

                doc.Load("EasySaleDbMySql.dll.config");

                XmlNode node = doc.SelectSingleNode(xPath);
                return node.Attributes["connectionString"].Value.ToString();
            }
            catch (Exception ex)
            {
                _log.Error("read config file error:" + ex.StackTrace);
              return "";
            }
        }

    }
}
