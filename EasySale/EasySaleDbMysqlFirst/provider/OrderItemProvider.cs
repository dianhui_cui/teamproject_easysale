﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
using EasyModel.database;
using EasySaleDbMysqlFirst.database;
namespace EasySaleDbMysqlFirst.provider
{
    public class OrderItemProvider : ProviderBase,IOrderItem
    {
        public OrderItemProvider(EasysaleEntities easysaleEntities) : base(easysaleEntities)
        { }

        public void Add(OrderItem orderItem)
        {
            DbOrderItem dbOrderItem = null;
            DataCopier.ToDbObject(orderItem, ref dbOrderItem);
            DbContext.OrderItems.Add(dbOrderItem);
            DbContext.SaveChanges();
            orderItem.ID = dbOrderItem.ID;
        }

        public List<OrderItem> FindByOrder(Order order)
        {
            return DataCopier.FromDbObject(DbContext.Orders.Find(order.ID).OrderItems.ToList());
        }

        public List<OrderItem> FindByOrderID(int orderID)
        {
            return DataCopier.FromDbObject(DbContext.Orders.Find(orderID).OrderItems.ToList());
        }
    }
}
