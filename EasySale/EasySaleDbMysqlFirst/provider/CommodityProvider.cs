﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
using EasyModel.database;
using EasySaleDbMysqlFirst.database;
namespace EasySaleDbMysqlFirst.provider
{
    public class CommodityProvider : ProviderBase,ICommodity
    {
        public CommodityProvider(EasysaleEntities easysaleEntities) : base(easysaleEntities)
        { }

        public void Add(Commodity commodity)
        {
            DbCommodity dbCommodity=null;
            DataCopier.ToDbObject(commodity,ref dbCommodity);
            DbContext.Commodities.Add(dbCommodity);
            DbContext.SaveChanges();
            commodity.ID = dbCommodity.ID;
         
        }

        public List<Commodity> Find(CommodityType type)
        {
            return DataCopier.FromDbObject(DbContext.CommodityTypes.Find(type.TypeCode).Commodities.ToList());
        }

        public Commodity Find(int id)
        {
            return DataCopier.FromDbObject(DbContext.Commodities.Find(id));
   
        }

        public List<Commodity> FindAll()
        {
            return DataCopier.FromDbObject(DbContext.Commodities.ToList());
        }

        public Commodity FindByBarcode(string barcode)
        {
            try
            {
                return DataCopier.FromDbObject(DbContext.Commodities.First(comm => comm.Barcode == barcode));
            } 
            catch (ArgumentNullException ex)
            {
                return null;
            }catch (InvalidOperationException e)
            {
                return null;
            }
        }

        public Commodity FindByCommCode(int commCode)
        {
            try { 
            return DataCopier.FromDbObject(DbContext.Commodities.First(comm => comm.CommCode == commCode));
            }
            catch (ArgumentNullException ex)
            {
                return null;
            }
            catch (InvalidOperationException e)
            {
                return null;
            }
        }

        public List<Commodity> FindByName(string name)
        {
            return DataCopier.FromDbObject((from commodity in DbContext.Commodities where commodity.Name.ToLower().Contains(name.ToLower()) select commodity).ToList());
        }

        public byte[] GetPicture(int id)
        {
            DbCommodityPicture dbCommodityPicture = DbContext.CommodityPictures.Find(id);
            if (null == dbCommodityPicture) return null;
            return dbCommodityPicture.Picture;
        }

        public void SetPicture(int id, byte[] picture)
        {
            DbCommodityPicture dbCommodityPicture = DbContext.CommodityPictures.Find(id);
            if (null == dbCommodityPicture)
            {
                dbCommodityPicture = new DbCommodityPicture() { ID = id, Picture = picture };
            }
            else {
                dbCommodityPicture.Picture = picture;
            }
            DbContext.SaveChanges();
        }

        public void Update(Commodity commodity)
        {
            DbCommodity dbCommodity = DbContext.Commodities.Find(commodity.ID);
            DataCopier.ToDbObject(commodity, ref dbCommodity);
            DbContext.SaveChanges();
        }
    }
}
