﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
using EasyModel.database;
using EasySaleDbMysqlFirst.database;
namespace EasySaleDbMysqlFirst.provider
{
    public class CustomerProvider : ProviderBase,ICustomer
    {
        public CustomerProvider(EasysaleEntities easysaleEntities) : base(easysaleEntities)
        { }

        public void Add(Customer customer)
        {
            DbCustomer dbCustomer = null;
            DataCopier.ToDbObject(customer, ref dbCustomer);
            DbContext.Customers.Add(dbCustomer);
            DbContext.SaveChanges();
            customer.ID = dbCustomer.ID;
        }

        public Customer Find(int id)
        {
            return DataCopier.FromDbObject(DbContext.Customers.Find(id));
        }

        public Customer Find(string MemberCardNo)
        {
            try
            {
                return DataCopier.FromDbObject(DbContext.Customers.First(customer => customer.MemberCardNo == MemberCardNo));
            }
            catch (ArgumentNullException ex)
            {
                return null;
            }
            catch (InvalidOperationException e)
            {
                return null;
            }

        }

        public List<Customer> FindAll()
        {
            return DataCopier.FromDbObject(DbContext.Customers.ToList());
        }

        public void Update(Customer customer)
        {
            DbCustomer dbCustomer = DbContext.Customers.Find(customer.ID);
            DataCopier.ToDbObject(customer, ref dbCustomer);
            DbContext.SaveChanges();
        }
    }
}
