﻿using EasyModel.business;
using EasyModel.database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasySaleDbMysqlFirst.database;
namespace EasySaleDbMysqlFirst.provider
{
    public class SupplierProvider:ProviderBase,ISupplier
    {
        public SupplierProvider(EasysaleEntities easysaleEntities) : base(easysaleEntities)
        { }

        public void Add(Supplier supplier)
        {
            DbSupplier dbSupplier = null;
            DataCopier.ToDbObject(supplier, ref dbSupplier);
            DbContext.Suppliers.Add(dbSupplier);
            DbContext.SaveChanges();
            supplier.ID = dbSupplier.ID;
        }

        public Supplier Find(int id)
        {
            return DataCopier.FromDbObject(DbContext.Suppliers.Find(id));
        }

        public List<Supplier> FindAll()
        {
            return DataCopier.FromDbObject(DbContext.Suppliers.ToList());
        }

        public void Update(Supplier supplier)
        {
            DbSupplier dbSupplier = DbContext.Suppliers.Find(supplier.ID);
            DataCopier.ToDbObject(supplier, ref dbSupplier);
            DbContext.SaveChanges();
        }
    }
}
