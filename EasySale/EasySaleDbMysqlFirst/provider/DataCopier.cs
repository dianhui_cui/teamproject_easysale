﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyModel.business;
using EasySaleDbMysqlFirst.database;

namespace EasySaleDbMysqlFirst.provider
{
    public class DataCopier
    {
        public static EasysaleEntities DBCONTEXT;

        public static void ToDbObject(Commodity commodity,ref DbCommodity dbCommodity)
        {
            if (dbCommodity == null)
            {
                dbCommodity = new DbCommodity();
                if (commodity.MemberPrice > 0)
                {
                    dbCommodity.MemberPrice = Math.Round(commodity.MemberPrice,2);
                }
                dbCommodity.Name = commodity.Name;
                //dbCommodity.Picture = commodity.Picture;
                dbCommodity.Price = Math.Round( commodity.Price,2);
                dbCommodity.Quantity = Math.Round(commodity.Quantity,2);
                dbCommodity.Unit = Enum.GetName(typeof(CommodityUnit), commodity.Unit); 
                dbCommodity.CommodityType = DBCONTEXT.CommodityTypes.Find(commodity.CommType);
                dbCommodity.CommType = commodity.CommType;
                if (commodity.CommCode > 0)
                {
                    dbCommodity.CommCode = commodity.CommCode;
                }
                if (!string.IsNullOrWhiteSpace(commodity.Barcode))
                {
                    dbCommodity.Barcode = commodity.Barcode;
                }

            }
            else
            {
                if (commodity.MemberPrice > 0)
                {
                    if (dbCommodity.MemberPrice != commodity.MemberPrice) dbCommodity.MemberPrice = Math.Round(commodity.MemberPrice,2);
                }
                if (dbCommodity.Name != commodity.Name) dbCommodity.Name = commodity.Name;
                //if (dbCommodity.Picture != commodity.Picture) dbCommodity.Picture = commodity.Picture;
                if (dbCommodity.Price != commodity.Price) dbCommodity.Price = Math.Round(commodity.Price,2);
                if (dbCommodity.Quantity != commodity.Quantity) dbCommodity.Quantity = Math.Round(commodity.Quantity,2);
                if (dbCommodity.Unit != Enum.GetName(typeof(CommodityUnit), commodity.Unit)) dbCommodity.Unit = Enum.GetName(typeof(CommodityUnit), commodity.Unit);
                if (dbCommodity.CommType != commodity.CommType)
                {
                    dbCommodity.CommodityType = DBCONTEXT.CommodityTypes.Find(commodity.CommType);
                    dbCommodity.CommType = commodity.CommType;
                }
                if (dbCommodity.CommCode != commodity.CommCode)
                {
                    if (commodity.CommCode > 0)
                    {
                        dbCommodity.CommCode = commodity.CommCode;
                    }
                    else
                    {
                        dbCommodity.CommCode = null;
                    }
                }
                if (dbCommodity.Barcode != commodity.Barcode)
                {
                    if (string.IsNullOrWhiteSpace(commodity.Barcode))
                    {
                        dbCommodity.Barcode = commodity.Barcode;
                    }
                    else
                    {
                        dbCommodity.Barcode = null;
                    }

                }
            }
         
        }
        public static void ToDbObject(CommodityType commodityType, ref DbCommodityType dbCommodityType)
        {
            if (dbCommodityType == null)
            {
                dbCommodityType = new DbCommodityType();
                dbCommodityType.TypeCode = commodityType.TypeCode;
                dbCommodityType.TypeName = commodityType.TypeName;
                if (commodityType.ParentCode > 0)
                    dbCommodityType.ParentType = DBCONTEXT.CommodityTypes.Find(commodityType.ParentCode);

            }
            else
            {
                if (dbCommodityType.TypeName != commodityType.TypeName) dbCommodityType.TypeName = commodityType.TypeName;
            }


        }
        public static void ToDbObject(Supplier supplier,ref DbSupplier dbSupplier)
        {
            if (dbSupplier == null)
            {
                dbSupplier = new DbSupplier();
                dbSupplier.Address = supplier.Address;
                dbSupplier.City = supplier.City;
                dbSupplier.Company = supplier.Company;
                dbSupplier.ContactName = supplier.ContactName;
                dbSupplier.Country = supplier.Country;
                dbSupplier.EMail = supplier.Email;
                dbSupplier.Memo = supplier.Memo;
                dbSupplier.PostalCode = supplier.PostalCode;
                dbSupplier.ProductTypes = supplier.ProductTypes;
                dbSupplier.Province = supplier.Province;
                dbSupplier.Telephone = supplier.Telephone;
            }
            else {
                if (dbSupplier.Address != supplier.Address) dbSupplier.Address = supplier.Address;
                if (dbSupplier.City != supplier.City) dbSupplier.City = supplier.City;
                if (dbSupplier.Company != supplier.Company) dbSupplier.Company = supplier.Company;
                if (dbSupplier.ContactName != supplier.ContactName) dbSupplier.ContactName = supplier.ContactName;
                if (dbSupplier.Country != supplier.Country) dbSupplier.Country = supplier.Country;
                if (dbSupplier.EMail != supplier.Email) dbSupplier.EMail = supplier.Email;
                if (dbSupplier.Memo != supplier.Memo) dbSupplier.Memo = supplier.Memo;
                if (dbSupplier.PostalCode != supplier.PostalCode) dbSupplier.PostalCode = supplier.PostalCode;
                if (dbSupplier.ProductTypes != supplier.ProductTypes) dbSupplier.ProductTypes = supplier.ProductTypes;
                if (dbSupplier.Province != supplier.Province) dbSupplier.Province = supplier.Province;
                if (dbSupplier.Telephone != supplier.Telephone) dbSupplier.Telephone = supplier.Telephone;
            }
           
        }
        public static void ToDbObject(Purchase purchase,ref DbPurchase dbPurchase)
        {
            if (dbPurchase == null)
            {
                dbPurchase = new DbPurchase();
                dbPurchase.CommID = purchase.CommID;
                dbPurchase.CommNumber = Math.Round(purchase.CommNumber,2);
                dbPurchase.Price = Math.Round(purchase.Price, 2);
                dbPurchase.Commodity = DBCONTEXT.Commodities.First(comm => comm.ID == purchase.CommID);
                dbPurchase.OrderDate = purchase.OrderDate;
                if (purchase.IsPurchased)
                {
                    if (purchase.PurchaseDate != null)
                    {
                        dbPurchase.PurchaseDate = purchase.PurchaseDate;
                    }
                    else
                    {
                        purchase.IsPurchased = false;
                    }
                }
              
                dbPurchase.Total = Math.Round(purchase.Total,2);
                dbPurchase.Supplier = DBCONTEXT.Suppliers.Find(purchase.SupplierID);
                dbPurchase.SupplierID = purchase.SupplierID;
            }
            else
            {
                if (dbPurchase.CommID != purchase.CommID) dbPurchase.CommID = purchase.CommID;
                if (dbPurchase.Price != purchase.Price) dbPurchase.Price = Math.Round(purchase.Price, 2);
                if (dbPurchase.CommNumber != purchase.CommNumber) dbPurchase.CommNumber = Math.Round(purchase.CommNumber,2);
                if (dbPurchase.CommID != purchase.CommID) dbPurchase.Commodity = DBCONTEXT.Commodities.Find(purchase.CommID);
                if (dbPurchase.OrderDate != purchase.OrderDate) dbPurchase.OrderDate = purchase.OrderDate;
                if (dbPurchase.PurchaseDate == null&&purchase.IsPurchased&&purchase.PurchaseDate!=null)
                {
                    dbPurchase.PurchaseDate = purchase.PurchaseDate;
                }
                if (dbPurchase.Total != purchase.Total) dbPurchase.Total = Math.Round(purchase.Total,2);
                if (dbPurchase.SupplierID != purchase.SupplierID)
                {
                    dbPurchase.Supplier = DBCONTEXT.Suppliers.Find(purchase.SupplierID);
                    dbPurchase.SupplierID = purchase.SupplierID;
                }
            }

        }
        public static void ToDbObject(Customer customer,ref DbCustomer dbCustomer)
        {
            if (dbCustomer == null)
            {
                dbCustomer = new DbCustomer();
                dbCustomer.Address = customer.Address;
                dbCustomer.Balance = customer.Balance;
                dbCustomer.City = customer.City;
                dbCustomer.Country = customer.Country;
                dbCustomer.Email = customer.Email;
                dbCustomer.FirstName = customer.FirstName;
                dbCustomer.LastName = customer.LastName;
                dbCustomer.MemberCardNo = customer.MemberCardNo;
                dbCustomer.PostalCode = customer.PostalCode;
                dbCustomer.Province = customer.Province;
                dbCustomer.Telephone = customer.Telephone;
            }
            else {

                if (dbCustomer.Address != customer.Address) dbCustomer.Address = customer.Address;
                if (dbCustomer.Balance != customer.Balance) dbCustomer.Balance = customer.Balance;
                if (dbCustomer.City != customer.City) dbCustomer.City = customer.City;
                if (dbCustomer.Country != customer.Country) dbCustomer.Country = customer.Country;
                if (dbCustomer.Email != customer.Email) dbCustomer.Email = customer.Email;
                if (dbCustomer.FirstName != customer.FirstName) dbCustomer.FirstName = customer.FirstName;
                if (dbCustomer.LastName != customer.LastName) dbCustomer.LastName = customer.LastName;
                if (dbCustomer.MemberCardNo != customer.MemberCardNo) dbCustomer.MemberCardNo = customer.MemberCardNo;
                if (dbCustomer.PostalCode != customer.PostalCode) dbCustomer.PostalCode = customer.PostalCode;
                if (dbCustomer.Province != customer.Province) dbCustomer.Province = customer.Province;
                if (dbCustomer.Telephone != customer.Telephone) dbCustomer.Telephone = customer.Telephone;
            }
           
           
        }
        public static void ToDbObject(Order order,ref DbOrder dbOrder)
        {
            if (dbOrder == null)
            {
                dbOrder = new DbOrder();
                if (order.CustomerID > 0)
                {
                    dbOrder.CustomerID = order.CustomerID;
                    dbOrder.Customer = DBCONTEXT.Customers.First(customer => customer.ID == order.CustomerID);
                }
                dbOrder.OrderTime = order.OrderTime;
                dbOrder.Total = Math.Round(order.Total,2);

            }
            else 
            {

                if (dbOrder.OrderTime != order.OrderTime) dbOrder.OrderTime = order.OrderTime;
                if (dbOrder.Total != order.Total) dbOrder.Total = Math.Round(order.Total,2);
            }
        }
        public static void ToDbObject(OrderItem orderItem,ref DbOrderItem dbOrderItem)
        {
            if (dbOrderItem == null)
            {
                dbOrderItem = new DbOrderItem();
                dbOrderItem.CommID = orderItem.CommID;
                dbOrderItem.Commodity = DBCONTEXT.Commodities.First(comm => comm.ID == orderItem.CommID);
                dbOrderItem.ItemCount = Math.Round(orderItem.ItemCount,2);
                if (orderItem.OrderID > 0)
                {
                    dbOrderItem.OrderID = orderItem.OrderID;
                    dbOrderItem.Order = DBCONTEXT.Orders.Find(orderItem.OrderID);
                }
                dbOrderItem.SubTotal = Math.Round(orderItem.SubTotal,2);
                dbOrderItem.UnitPrice = Math.Round(orderItem.UnitPrice,2);
                dbOrderItem.GST = Math.Round(orderItem.GST,2);
                dbOrderItem.PST = Math.Round(orderItem.PST,2);
            }
            else
            {
                if (dbOrderItem.ItemCount != orderItem.ItemCount) dbOrderItem.ItemCount = Math.Round(orderItem.ItemCount,2);
                if (dbOrderItem.SubTotal != orderItem.SubTotal) dbOrderItem.SubTotal = Math.Round(orderItem.SubTotal,2);
                if (dbOrderItem.UnitPrice != orderItem.UnitPrice) dbOrderItem.UnitPrice = Math.Round(orderItem.UnitPrice,2);
                if (dbOrderItem.GST != orderItem.GST) dbOrderItem.GST = Math.Round(orderItem.GST,2);
                if (dbOrderItem.PST != orderItem.PST) dbOrderItem.PST = Math.Round(orderItem.PST,2);

            }
        }
        public static Commodity FromDbObject(DbCommodity dbCommodity)
        {
            if (null == dbCommodity) return null;
            Commodity commodity = new Commodity()
            {
                ID = dbCommodity.ID,
                Barcode = dbCommodity.Barcode,
                CommCode = dbCommodity.CommCode == null ? 0 : dbCommodity.CommCode.Value,
                CommType = dbCommodity.CommodityType.TypeCode,
                CommTypeName = dbCommodity.CommodityType.TypeName,
                MemberPrice = dbCommodity.MemberPrice == null ? 0 : dbCommodity.MemberPrice.Value,
                Name = dbCommodity.Name,
                //Picture = dbCommodity.Picture,
                Price = dbCommodity.Price,
                Quantity = dbCommodity.Quantity,
                Unit = (CommodityUnit)Enum.Parse(typeof(CommodityUnit), dbCommodity.Unit)
                
            };
            return commodity;
        }
        public static CommodityType FromDbObject(DbCommodityType dbCommodityType)
        {
            if (null == dbCommodityType) return null;
            CommodityType commodityType = new CommodityType()
            {
                TypeCode = dbCommodityType.TypeCode,
                TypeName=dbCommodityType.TypeName,
                ParentCode=dbCommodityType.ParentType==null?0:dbCommodityType.ParentType.TypeCode,
                GST=dbCommodityType.GST,
                PST=dbCommodityType.PST
                
            };
            return commodityType;
        }
        public static Supplier FromDbObject(DbSupplier dbSupplier)
        {
            if (null == dbSupplier) return null;
            Supplier supplier = new Supplier()
            { 
                ID=dbSupplier.ID,
                Address=dbSupplier.Address,
                City=dbSupplier.City,
                Company=dbSupplier.Company,
                ContactName=dbSupplier.ContactName,
                Country=dbSupplier.Country,
                Email=dbSupplier.EMail,
                Memo=dbSupplier.Memo,
                PostalCode=dbSupplier.PostalCode,
                ProductTypes=dbSupplier.ProductTypes,
                Province=dbSupplier.Province,
                Telephone=dbSupplier.Telephone
            };
            return supplier;
        }
        public static Purchase FromDbObject(DbPurchase dbPurchase)
        {
            if (null == dbPurchase) return null;
            Purchase purchase = new Purchase()
            {
                ID = dbPurchase.ID,
                CommID = dbPurchase.CommID,
                CommName = dbPurchase.Commodity.Name,
                CommNumber = dbPurchase.CommNumber,
                IsPurchased = dbPurchase.PurchaseDate == null ? false : true,
                OrderDate = dbPurchase.OrderDate,
                Price = dbPurchase.Price,
                PurchaseDate = dbPurchase.PurchaseDate,
                SupplierID = dbPurchase.SupplierID,
                SupplierName = dbPurchase.Supplier.Company,
                Total = dbPurchase.Total
            };
            return purchase;
        }
        public static Customer FromDbObject(DbCustomer dbCustomer)
        {
            if (null == dbCustomer) return null;
            Customer customer = new Customer()
            { 
                ID=dbCustomer.ID,
                Address=dbCustomer.Address,
                Balance=dbCustomer.Balance,
                City=dbCustomer.City,
                Country=dbCustomer.Country,
                Email=dbCustomer.Email,
                FirstName=dbCustomer.FirstName,
                LastName=dbCustomer.LastName,
                MemberCardNo=dbCustomer.MemberCardNo,
                PostalCode=dbCustomer.PostalCode,
                Province=dbCustomer.Province,
                Telephone=dbCustomer.Telephone
            };
            return customer;
        }
        public static Order FromDbObject(DbOrder dbOrder)
        {
            if (null == dbOrder) return null;
            
            Order order = new Order()
            { 
                ID=dbOrder.ID,
                
                CustomerFullName=dbOrder.Customer==null?"":string.Format("{0} {1}", dbOrder.Customer.FirstName,dbOrder.Customer.LastName),
                CustomerID=dbOrder.Customer==null?0:dbOrder.CustomerID.Value,
                OrderTime=dbOrder.OrderTime,
                Total=dbOrder.Total
            };
            return order;
        }
        public static OrderItem FromDbObject(DbOrderItem dbOrderItem)
        {
            if (null == dbOrderItem) return null;
            OrderItem orderItem = new OrderItem()
            { 
                ID=dbOrderItem.ID,
                OrderID=dbOrderItem.OrderID,
                CommID=dbOrderItem.CommID,
                CommName=dbOrderItem.Commodity.Name,
                ItemCount=dbOrderItem.ItemCount,
                SubTotal=dbOrderItem.SubTotal,
                UnitPrice=dbOrderItem.UnitPrice,
                GST=dbOrderItem.GST,
                PST=dbOrderItem.PST

            };
            return orderItem;
        }
        public static List<Commodity> FromDbObject(List<DbCommodity> dbCommodities)
        {
            return (from comm in dbCommodities select FromDbObject(comm)).ToList();
        }
        public static List<CommodityType> FromDbObject(List<DbCommodityType> dbCommodityTypes)
        {
            return (from type in dbCommodityTypes select FromDbObject(type)).ToList();
        }
        public static List<Supplier> FromDbObject(List<DbSupplier> dbSuppliers)
        {
            return (from supplier in dbSuppliers select FromDbObject(supplier)).ToList();
        }
        public static List<Purchase> FromDbObject(List<DbPurchase> dbPurchases)
        {
            return (from purchase in dbPurchases select FromDbObject(purchase)).ToList();
        }
        public static List<Customer> FromDbObject(List<DbCustomer> dbCustomers)
        {
            return (from customer in dbCustomers select FromDbObject(customer)).ToList();
        }
        public static List<Order> FromDbObject(List<DbOrder> dbOrders)
        {
            return (from order in dbOrders select FromDbObject(order)).ToList();
        }
        public static List<OrderItem> FromDbObject(List<DbOrderItem> dbOrderItems)
        {
            return (from orderItem in dbOrderItems select FromDbObject(orderItem)).ToList();
        }
    }
}
