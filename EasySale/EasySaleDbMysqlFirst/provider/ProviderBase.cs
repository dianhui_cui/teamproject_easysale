﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasySaleDbMysqlFirst.database;

namespace EasySaleDbMysqlFirst.provider
{
    public abstract class ProviderBase
    {
        private EasysaleEntities _easysaleEntities;
      
        public ProviderBase(EasysaleEntities easySaleDb)
        {
            _easysaleEntities = easySaleDb;
        }
        public EasysaleEntities DbContext
        {
            get { return _easysaleEntities; }
        }
    }
}
