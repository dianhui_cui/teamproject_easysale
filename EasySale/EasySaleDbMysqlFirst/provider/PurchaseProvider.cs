﻿using EasyModel.business;
using EasyModel.database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasySaleDbMysqlFirst.database;
namespace EasySaleDbMysqlFirst.provider
{
    public class PurchaseProvider : ProviderBase,IPurchase
    {
        public PurchaseProvider(EasysaleEntities easysaleEntities) : base(easysaleEntities)
        { }

        public void Add(Purchase purchase)
        {
            DbPurchase dbPurchase = null;
            DataCopier.ToDbObject(purchase, ref dbPurchase);
            DbContext.Purchases.Add(dbPurchase);
            if (purchase.IsPurchased&&purchase.PurchaseDate!=null)
            {

                DbCommodity dbCommodity = DbContext.Commodities.Find(purchase.CommID);
                dbCommodity.Quantity = dbCommodity.Quantity + dbPurchase.CommNumber;
            }
            DbContext.SaveChanges();
            purchase.ID = dbPurchase.ID;
        }
        public void Update(Purchase purchase)
        {

            DbPurchase dbPurchase = DbContext.Purchases.Find(purchase.ID);

            bool dbPurchased = dbPurchase.PurchaseDate == null ? false : true;
            bool purchased = purchase.IsPurchased;

            DataCopier.ToDbObject(purchase, ref dbPurchase);
            if (!dbPurchased && purchased && purchase.PurchaseDate != null)
            {
                DbCommodity dbCommodity = DbContext.Commodities.Find(purchase.CommID);
                dbCommodity.Quantity = dbCommodity.Quantity + dbPurchase.CommNumber;
            }
            DbContext.SaveChanges();
        }
        public void Add(List<Purchase> purchases)
        {
            foreach (var purchase in purchases)
            {
                DbPurchase dbPurchase = null;
                DataCopier.ToDbObject(purchase, ref dbPurchase);
                DbContext.Purchases.Add(dbPurchase);
                if (purchase.IsPurchased && purchase.PurchaseDate != null)
                {
                    DbCommodity dbCommodity = DbContext.Commodities.Find(purchase.CommID);
                    dbCommodity.Quantity = dbCommodity.Quantity + dbPurchase.CommNumber;
                }
            }
          
            DbContext.SaveChanges();
        }

        public Purchase Find(int id)
        {
            return DataCopier.FromDbObject(DbContext.Purchases.Find(id));
        }
        public List<Purchase> FindAll()
        {
            return DataCopier.FromDbObject(DbContext.Purchases.ToList());
        }
         public List<Purchase> FindAll(bool isCompleted)
        {
            if (isCompleted)
            {
                return DataCopier.FromDbObject((from purchase in DbContext.Purchases where purchase.PurchaseDate != null select purchase).ToList());
            }
            else
            {
                return DataCopier.FromDbObject((from purchase in DbContext.Purchases where purchase.PurchaseDate == null select purchase).ToList());
            }
        }

        public List<Purchase> findBySupplier(int supplierID)
        {
            DbSupplier dbSupplier = DbContext.Suppliers.Find(supplierID);
            if (dbSupplier == null) return new List<Purchase>();
            return DataCopier.FromDbObject(dbSupplier.Purchases.ToList());
        }

        public List<Purchase> findBySupplier(int supplierID, bool isCompleted)
        {
            if (isCompleted)
            {
                return DataCopier.FromDbObject((from purchase in DbContext.Purchases where purchase.PurchaseDate != null &&purchase.SupplierID==supplierID select purchase).ToList());
            }
            else
            {
                return DataCopier.FromDbObject((from purchase in DbContext.Purchases where purchase.PurchaseDate == null && purchase.SupplierID == supplierID select purchase).ToList());
            }
        }

    
    }
}
