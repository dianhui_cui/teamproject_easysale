﻿using EasyModel.database;
using EasySaleDbMysqlFirst.provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasySaleDbMysqlFirst.database;
using System.Reflection;
using System.Configuration;
using MySql.Data.MySqlClient;

namespace EasySaleDbMysqlFirst
{
    public class EasySaleDatabaseMySql : IEasyDatabase
    {
        private EasysaleEntities easysaleEntities;
       
        private MySqlConnection connection;
        public EasySaleDatabaseMySql()
        {
            //string connectionstring = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location).AppSettings.Settings["MysqlConnectionString"].Value;
            //connection = new MySqlConnection(connectionstring);
            //connection.Open();




            easysaleEntities = new EasysaleEntities();
            DataCopier.DBCONTEXT = easysaleEntities;
            this.SupplierOperator = new SupplierProvider(easysaleEntities);
            this.OrderOperator = new OrderProvider(easysaleEntities);
            this.OrderItemOperator = new OrderItemProvider(easysaleEntities);
            this.PurchaseOperator = new PurchaseProvider(easysaleEntities);
            this.CommodityOperator = new CommodityProvider(easysaleEntities);
            this.CommodityTypeOperator = new CommodityTypeProvider(easysaleEntities);
            this.CustomerOperator = new CustomerProvider(easysaleEntities);
        }
        public IOrder OrderOperator { get; set; }
        public IOrderItem OrderItemOperator { get; set; }
        public ISupplier SupplierOperator { get; set; }
        public ICustomer CustomerOperator { get; set; }
        public IPurchase PurchaseOperator { get; set; }
        public ICommodity CommodityOperator { get; set; }
        public ICommodityType CommodityTypeOperator { get; set; }
        public void Close()
        {
            //if(connection!=null)
            //    connection.Close();
        }
    }
}
